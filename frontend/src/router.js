// router.js
import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from './views/Login.vue'
import HomePage from './views/HomePage.vue'
import Config from './views/Config.vue'
//users
import NewUser from './views/users/NewUser.vue'
import ListUsers from './views/users/ListUsers.vue'
//clients
import NewClient from './views/clients/NewClient.vue'
import ListClients from './views/clients/ListClients.vue'
//providers
import NewProvider from './views/providers/NewProvider.vue'
import ListProviders from './views/providers/ListProviders.vue'
//hotels
import NewHotel from './views/hotels/NewHotel.vue'
import ListHotels from './views/hotels/ListHotels.vue'
import ChooseHotel from './views/hotels/ChooseHotel.vue'
//rooms
import NewRoomType from './views/rooms/NewRoomType.vue'
import ListRoomTypes from './views/rooms/ListRoomTypes.vue'
import NewAttribute from "./views/rooms/NewAttribute.vue";
import ListAttributes from "./views/rooms/ListAttributes.vue";
import ListRooms from "./views/rooms/ListRooms.vue";
import NewRoom from "./views/rooms/NewRoom.vue";
import Prices from "./views/rooms/Prices.vue";
//planing
import Planing from './views/planing/Planing.vue'
//invoices
import NewInvoice from "./views/invoices/NewInvoice.vue";
import ListInvoices from "./views/invoices/ListInvoices.vue";
import NewSerial from "./views/invoices/NewSerial.vue";
import ListSerials from "./views/invoices/ListSerials.vue";

import store from './store.js'

Vue.use(VueRouter)

const routes = [{ //dashboard
        component: HomePage,
        name: 'Home',
        path: '/home',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("Login page")
            next()
        }
    },
    { //login
        component: Login,
        name: 'Login',
        path: '/',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (store.getters['isAuth']) {
                return next({
                    name: 'Home'
                })
            }
            console.log("Home page")
            next()
        }
    },
    { //config grpc server
        component: Config,
        name: 'Config',
        path: '/configuration'
    },
    { //users routes
        component: NewUser,
        name: 'NewUser',
        path: '/users/new/:id?',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("New user page")
            next()
        }
    },
    {
        component: ListUsers,
        name: 'ListUsers',
        path: '/users/list',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("List users page")
            next()
        }
    },
    { //clients routes
        component: NewClient,
        name: 'NewClient',
        path: '/clients/new/:id?',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("Client new page")
            next()
        }
    },
    {
        component: ListClients,
        name: 'ListClients',
        path: '/clients/list',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("List clients page")
            next()
        }
    },
    {
        component: ListProviders,
        name: 'ListProviders',
        path: '/providers/list',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("List providers page")
            next()
        }
    },
    {
        component: NewProvider,
        name: 'NewProvider',
        path: '/providers/new/:id?',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("New providers page")
            next()
        }
    },
    { //hotels
        component: NewHotel,
        name: 'NewHotel',
        path: '/hotels/new/:id?',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            } else {
                if (!store.getters['isAdmin']) {
                    return next({
                        name: 'Home'
                    })
                }
            }
            console.log("New Hotel page")
            next()
        }
    },
    {
        component: ListHotels,
        name: 'ListHotels',
        path: '/hotels/list',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            } else {
                if (!store.getters['isAdmin']) {
                    return next({
                        name: 'Home'
                    })
                }
            }
            console.log("List Hotels page")
            next()
        }
    },
    {
        component: ChooseHotel,
        name: 'ChooseHotel',
        path: '/hotels/choose',
        /*
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("Choose Hotel page")
            next()
        }
        */
    },
    { //planing
        component: Planing,
        name: 'Planing',
        path: '/planing',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("Planing page")
            next()
        }
    },
    { //room types
        component: NewRoomType,
        name: 'NewRoomType',
        path: '/room-types/new/:id?',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("Room type page")
            next()
        }
    },
    { //room types
        component: ListRoomTypes,
        name: 'ListRoomTypes',
        path: '/room-types/list',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("List Room types page")
            next()
        }
    },
    { //room attributes
        component: ListAttributes,
        name: 'ListAttributes',
        path: '/room-attributes/list',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("List attributes room page")
            next()
        }
    },
    {
        component: NewAttribute,
        name: 'NewAttribute',
        path: '/room-attributes/new/:id?',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("New attributes room page")
            next()
        }
    },
    { //rooms
        component: ListRooms,
        name: 'ListRooms',
        path: '/rooms/list',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("List rooms page")
            next()
        }
    },
    {
        component: NewRoom,
        name: 'NewRoom',
        path: '/rooms/new/:id?',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("Rooms page")
            next()
        }
    },
    {
        component: Prices,
        name: 'Prices',
        path: '/rooms/prices/:id',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("Rooms prices page")
            next()
        }
    },
    {
        component: ListInvoices,
        name: 'ListInvoices',
        path: '/invoices/list',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("List invoices page")
            next()
        }
    },
    {
        component: NewInvoice,
        name: 'NewInvoice',
        path: '/invoices/new/:id?',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("New invoice page")
            next()
        }
    },
    {
        component: ListSerials,
        name: 'ListSerials',
        path: '/invoices/list-serials',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("List invoices serials page")
            next()
        }
    },
    {
        component: NewSerial,
        name: 'NewSerial',
        path: '/invoices/new-serial/:id?',
        beforeEnter: (to, from, next) => {
            console.log(store.getters)
            if (!store.getters['isAuth']) {
                return next({
                    name: 'Login'
                })
            }
            console.log("New invoice serial page")
            next()
        }
    }
]

const router = new VueRouter({
    mode: 'abstract', // mode must be set to 'abstract'
    routes,
})

export default router