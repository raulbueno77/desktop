// store.js
import Vue from 'vue'
import Vuex from 'vuex'
import router from './router.js'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        software: {
            name: "ceHotel",
            version: "v1.0",
            connected: false,
        },
        session: {
            token: "",
            username: "",
            user_id: "",
            role: "",
            fullname: "",
            authenticated: false,
            hotel: {
                id: '',
                name: '',
            },
            message_text: "",
            message_error: false
        },
        menus: {
            mainMenu: true,
            hotelMenu: false,
        },
    },
    getters: {
        isAuth(state) { return state.session.authenticated },
        getToken(state) { return state.session.token },
        getRole(state) { return state.session.role },
        getUsername(state) { return state.session.username },
        getUserid(state) { return state.session.user_id },
        isConnected(state) { return state.software.connected },
        isAdmin(state) {
            if (state.session.role == "Admin") {
                return true
            } else {
                return false
            }
        }
    },
    mutations: {
        login(state) {
            console.log('authenticated!')
            state.session.authenticated = true
            router.push('/home')
        },
        logoutHotel(state) {
            state.session.hotel = {
                id: '',
                name: ''
            }

            router.push('/hotels/choose')
        },
        logout(state) {
            state.session.authenticated = false
            state.session.token = ''
            state.session.username = ''
            state.session.role = ''
            state.session.hotel.name = ''
            state.session.hotel.id = ''
            router.push('/')
        },
        showMainMenu(state) {
            state.menus.mainMenu = true
            state.menus.hotelMenu = false
        },
        showHotelMenu(state) {
            state.menus.mainMenu = false
            state.menus.hotelMenu = true
        },
        clearMessage(state) {
            state.session.message_text = ""
            state.session.message_error = false
        }
    },
    actions: {
        login({ commit }) {
            commit('login')
        },
        logout({ commit }) {
            commit('logout')
        },
        logoutHotel({ commit }) {
            commit('logoutHotel')
        },
        showMainMenu({ commit }) {
            commit('showMainMenu')
        },
        showHotelMenu({ commit }) {
            commit('showHotelMenu')
        },
        clearMessage({ commit }) {
            commit('clearMessage')
        }
    }
})

export default store