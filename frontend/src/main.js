// main.js
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue'
import App from './App.vue'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'


// Install BootstrapVue
Vue.use(BootstrapVue)
    // Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// import the router
import router from './router'

import store from './store'

Vue.config.productionTip = false
Vue.config.devtools = true

import * as Wails from '@wailsapp/runtime'

Wails.Init(() => {
    new Vue({
        render: (h) => h(App),
        // add the router to the Vue constructor
        router,
        store,
        mounted() {
            this.$router.replace('/')
        },
    }).$mount('#app')
})