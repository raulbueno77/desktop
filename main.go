package main

import (
	_ "embed"
	"cehotel/backend/services/bookings"
	"cehotel/backend/services/clients"
	"cehotel/backend/services/crons"
	grpc2 "cehotel/backend/services/grpc"
	"cehotel/backend/services/hotels"
	"cehotel/backend/services/providers"
	"cehotel/backend/services/reports"
	"cehotel/backend/services/rooms"
	saleInvoices "cehotel/backend/services/sale_invoices"
	"cehotel/backend/services/users"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"

	"google.golang.org/grpc"

	"github.com/wailsapp/wails"
	"gopkg.in/ini.v1"
)

var cc *grpc.ClientConn
var app *wails.App
var hotelSelected string
//go:embed frontend/dist/app.js
var js string

//go:embed frontend/dist/app.css
var css string

type ReturnConfig struct {
	Host          string
	Port          string
	PdfExecutable string
}

func getDir() (string, error) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	log.Println(dir)
	if err != nil {
		return "", err
	}

	return dir, nil
}

func isWindows() bool {
	return os.PathSeparator == '\\' && os.PathListSeparator == ';'
}

func ConfigFile() (*string, *string, *string, error) {
	dir, err := getDir()

	if err != nil {
		log.Fatal(err)
	}

	host := "0.0.0.0"
	port := "50051"
	pdfExecutable := ""

	cfg, err := ini.Load(dir + "/config.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Create(dir + "/config.ini")
		cfg, _ := ini.Load(dir + "/config.ini")
		cfg.Section("grpc_server").Key("host").SetValue("0.0.0.0")
		cfg.Section("grpc_server").Key("port").SetValue("50051")
		if !isWindows() {
			cfg.Section("other_configuration").Key("pdf_executable").SetValue("/usr/bin/firefox")
			pdfExecutable = "/usr/bin/firefox"
		}
		cfg.SaveTo(dir + "/config.ini")
		return &host, &port, &pdfExecutable, nil
	} else {
		host = cfg.Section("grpc_server").Key("host").String()
		port = cfg.Section("grpc_server").Key("port").String()
		pdfExecutable = cfg.Section("other_configuration").Key("pdf_executable").String()
	}

	return &host, &port, &pdfExecutable, nil
}

func GetConfigurationGrpc() *ReturnConfig {
	dir, _ := getDir()

	cfg, _ := ini.Load(dir + "/config.ini")

	host := cfg.Section("grpc_server").Key("host").String()
	port := cfg.Section("grpc_server").Key("port").String()
	pdfExecutable := cfg.Section("other_configuration").Key("pdf_executable").String()

	log.Println(pdfExecutable)

	return &ReturnConfig{
		Host:          host,
		Port:          port,
		PdfExecutable: pdfExecutable,
	}
}

func SaveConfigurationGrpc(host string, port string, pdfExecutable string) bool {
	dir, _ := getDir()

	cfg, _ := ini.Load(dir + "/config.ini")

	cfg.Section("grpc_server").Key("host").SetValue(host)
	cfg.Section("grpc_server").Key("port").SetValue(port)
	cfg.Section("other_configuration").Key("pdf_executable").SetValue(pdfExecutable)
	cfg.SaveTo(dir + "/config.ini")

	return true
}

func SetChoosenHotel(hotelId string) {
	hotelSelected = hotelId
}

func GetChoosenHotel() string {
	return hotelSelected
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	separatorDir := "/"

	if runtime.GOOS == "windows" {
		separatorDir = "\\"
	}

	host, port, pdfExecutable, _ := ConfigFile()

	grpcCli := grpc2.NewGrpcClient(*host, *port)

	cc, err := grpcCli.CreateCC()

	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}

	defer cc.Close()

	user := users.NewUserClient(cc)
	hotel := hotels.NewHotelClient(cc)
	client := clients.NewClientsClient(cc)
	room := rooms.NewRoomClient(cc)
	provider := providers.NewProvidersClient(cc)
	booking := bookings.NewBookingClient(cc)
	report := reports.NewReportClient(cc, *pdfExecutable, separatorDir)
	saleInvoice := saleInvoices.NewSaleInvoiceClient(cc)

	crontabs := crons.NewCrons()

	go crontabs.ClearTemps()

	//test the connection
	connection := user.TestConnection()

	if connection {
		log.Println("Conection success to grpc server")
	} else {
		log.Println("Error connecting to grpc server")
	}

	/*
	js := mewn.String("./frontend/dist/app.js")
	css := mewn.String("./frontend/dist/app.css")
	*/

	app = wails.CreateApp(&wails.AppConfig{
		Width:     1280,
		Height:    900,
		Title:     "ceHotel",
		JS:        js,
		CSS:       css,
		Colour:    "#131313",
		Resizable: true,
	})

	app.Bind(GetConfigurationGrpc)
	app.Bind(SaveConfigurationGrpc)
	app.Bind(SetChoosenHotel)
	app.Bind(GetChoosenHotel)
	app.Bind(user)
	app.Bind(hotel)
	app.Bind(client)
	app.Bind(provider)
	app.Bind(room)
	app.Bind(booking)
	app.Bind(report)
	app.Bind(saleInvoice)
	app.Run()
}
