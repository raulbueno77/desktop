package reports

import (
	bookingpb "cehotel/backend/proto/booking"
	hotelpb "cehotel/backend/proto/hotel"
	roompb "cehotel/backend/proto/room"
	"cehotel/backend/utils"
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/johnfercher/maroto/pkg/color"
	"github.com/johnfercher/maroto/pkg/consts"
	"github.com/johnfercher/maroto/pkg/pdf"
	"github.com/johnfercher/maroto/pkg/props"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type ReportClient struct {
	serviceBooking bookingpb.BookingServicesClient
	serviceHotel   hotelpb.HotelServicesClient
	serviceRoom    roompb.RoomServicesClient
	pdfExecutable  string
	separatorDir   string
}

func NewReportClient(cc *grpc.ClientConn, pdfExecutable string, separatorDir string) *ReportClient {
	return &ReportClient{
		serviceBooking: bookingpb.NewBookingServicesClient(cc),
		serviceHotel:   hotelpb.NewHotelServicesClient(cc),
		serviceRoom:    roompb.NewRoomServicesClient(cc),
		pdfExecutable:  pdfExecutable,
		separatorDir:   separatorDir,
	}
}

func (c *ReportClient) GenerateGuestDocument(guestId int32, token string) (*string, error) {
	log.Println("Generate Guest Document...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	os.Mkdir("temp", os.ModePerm)

	darkGrayColor := getDarkGrayColor()
	//grayColor := getGrayColor()
	whiteColor := color.NewWhite()
	header := []string{"", ""}
	//contents := getContents()

	m := pdf.NewMaroto(consts.Portrait, consts.A4)
	m.SetPageMargins(10, 15, 10)
	//m.SetBorder(true)

	respGuest, err := c.serviceBooking.GetGuest(ctx, &bookingpb.GetGuestRequest{
		Id:                   guestId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	guest := respGuest.GetGuest()

	respBooking, err := c.serviceBooking.GetBooking(ctx, &bookingpb.GetBookingRequest{
		Id:                   guest.GetBookingId(),
		RoomName:             "",
		Begin:                "",
		HotelId:              0,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	booking := respBooking.GetBooking()

	respRoom, err := c.serviceRoom.GetRoom(ctx, &roompb.GetRoomRequest{
		Id:                   booking.GetRoom().GetId(),
		Name:                 "",
		HotelId:              0,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	room := respRoom.GetRoom()

	respHotel, err := c.serviceHotel.GetHotel(ctx, &hotelpb.GetHotelRequest{
		Id:                   room.GetHotel().GetId(),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	hotel := respHotel.GetHotel()

	m.RegisterHeader(func() {
		m.Row(20, func() {
			if hotel.GetLogo() != "" {
				m.Col(3, func() {
					err = m.FileImage("media/"+hotel.GetLogo(), props.Rect{
						Center:  true,
						Percent: 80,
					})

					if err != nil {
						log.Printf("Image error: %v", err)
					}
				})
			} else {
				m.ColSpace(3)
			}

			m.ColSpace(4)

			year := func() string {
				date, err := time.Parse("2006-01-02", booking.GetDateBegin())

				if err != nil {
					return ""
				}

				return date.Format("2006")
			}()

			m.Col(5, func() {
				m.Text(fmt.Sprintf("Reserva Nº: %v/%v", booking.GetNumber(), year), props.Text{
					Size:        8,
					Style:       consts.Bold,
					Align:       consts.Right,
					Extrapolate: false,
				})
				m.Text(hotel.Name, props.Text{
					Top:   4,
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
				m.Text(hotel.Address+" "+hotel.PostalCode, props.Text{
					Top:   8,
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
				m.Text(hotel.City+" "+hotel.Region, props.Text{
					Top:   12,
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
				m.Text("Cif: "+hotel.Document, props.Text{
					Top:   16,
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
				m.Text("RTA: "+hotel.Reference, props.Text{
					Top:   20,
					Style: consts.BoldItalic,
					Size:  8,
					Align: consts.Right,
				})
			})
		})
	})

	/*
		m.RegisterFooter(func() {
			m.Row(20, func() {
				m.Col(12, func() {
					m.Text("Tel: 55 024 12345-1234", props.Text{
						Top:   13,
						Style: consts.BoldItalic,
						Size:  8,
						Align: consts.Left,
					})
					m.Text("www.mycompany.com", props.Text{
						Top:   16,
						Style: consts.BoldItalic,
						Size:  8,
						Align: consts.Left,
					})
				})
			})
		})
	*/
	m.Row(10, func() {
		m.Col(12, func() {
			m.Text("Modelo de parte de entrada de viajeros", props.Text{
				Top:   3,
				Style: consts.Bold,
				Align: consts.Center,
			})
		})
	})

	m.SetBackgroundColor(darkGrayColor)

	m.Row(7, func() {
		m.Col(3, func() {
			m.Text(" ", props.Text{
				Top:   1.5,
				Size:  9,
				Style: consts.Bold,
				Align: consts.Center,
			})
		})
		m.ColSpace(9)
	})

	m.SetBackgroundColor(whiteColor)

	contents := [][]string{
		{"Documento:", guest.GetDocument()},
		{"Tipo documento:", func() string {
			switch guest.GetDocumentType() {
			case "D":
				return "DNI"
			case "P":
				return "PASAPORTE"
			case "C":
				return "CARNE DE CONDUCIR"
			case "I":
				return "CARTA O DOCUMENTO DE IDENTIDAD"
			case "N":
				return "PERMISO DE RESIDENCIA ESPAÑOL"
			case "X":
				return "PERMISO DE RESIDENCIA UE"
			}

			return ""
		}()},
		{"Fecha expedición documento:", func() string {
			date, err := time.Parse("2006-01-02", guest.GetDocumentDate())

			if err != nil {
				return ""
			}

			return date.Format("20060102")
		}()},
		{"Primer Apellido:", strings.ToUpper(guest.GetLastName())},
		{"Segundo Apellido:", strings.ToUpper(guest.GetSecondLastName())},
		{"Nombre:", strings.ToUpper(guest.GetFirstName())},
		{"Sexo", func() string {
			if guest.GetGender() == "M" {
				return "MASCULINO"
			}

			return "FEMENINO"
		}()},
		{"Fecha de nacimiento:", func() string {
			date, err := time.Parse("2006-01-02", guest.GetBirthDate())

			if err != nil {
				return ""
			}

			return date.Format("20060102")
		}()},
		{"País:", strings.ToUpper(guest.GetCountry())},
		{"Fecha de entrada:", func() string {
			date, err := time.Parse("2006-01-02", guest.GetCheckIn())

			if err != nil {
				return ""
			}

			return date.Format("20060102")
		}()},
	}

	log.Println(contents)
	m.TableList(header, contents, props.TableList{
		HeaderProp: props.TableListContent{
			Size:      10,
			GridSizes: []uint{4, 8},
		},
		ContentProp: props.TableListContent{
			Size:      10,
			GridSizes: []uint{4, 8},
		},
		Align:                consts.Left,
		AlternatedBackground: &whiteColor,
		HeaderContentSpace:   1,
		Line:                 false,
	})

	m.Row(20, func() {
		m.Col(12, func() {
			m.Text("_________________________ , _______ de ____________________ de 20____", props.Text{
				Top:   20,
				Style: consts.Normal,
				Align: consts.Center,
			})
		})
	})

	m.Row(60, func() {
		m.Col(12, func() {
			m.Text("Firma del huésped", props.Text{
				Top:   60,
				Style: consts.Normal,
				Align: consts.Center,
			})
		})
	})

	m.Row(90, func() {
		m.Col(12, func() {
			m.Text("La recogida y tratamiento se hará de acuerdo con la Ley Orgánica 15/1999, de 13 de Diciembre, de Protección de Datos de Caracter Personal y al amparo de lo dispuesto en el artículo 12.1 de la Ley Orgánica 1/1992, de 21 de Febrero, sobre Protección de la Seguridad Ciudadana.", props.Text{
				Top:   90,
				Style: consts.Normal,
				Align: consts.Center,
				Size:  8,
			})
		})
	})

	fileName := utils.RandomString(15) + ".pdf"

	err = m.OutputFileAndClose("temp/" + fileName)

	if err != nil {
		return nil, err
	}

	//f, err := os.Open("temp/" + fileName)

	//if err != nil {
	//	return nil, nil
	//}

	// Read entire file into byte slice.
	//reader := bufio.NewReader(f)
	//content, _ := ioutil.ReadAll(reader)

	// Encode as base64.
	//encoded := "data:application/pdf;base64," + base64.StdEncoding.EncodeToString(content)

	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	log.Println(dir)
	//utils.ExecuteShell(c.pdfExecutable, "/home/gutierrez/gocode/src/ceginfor/cehotel/temp/" + fileName)
	_, err = utils.ExecuteShell(c.pdfExecutable, dir+c.separatorDir+"temp"+c.separatorDir+fileName)

	if err != nil {
		log.Println(err)
	}

	toReturn := "operation ok!!!"

	return &toReturn, nil
}

func getDarkGrayColor() color.Color {
	return color.Color{
		Red:   144,
		Green: 144,
		Blue:  144,
	}
}
