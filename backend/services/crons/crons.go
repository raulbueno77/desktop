package crons

import (
	"io/ioutil"
	"log"
	"os"
	"time"
)

type Crons struct {

}

func NewCrons() *Crons {
	return &Crons{}
}

func (c *Crons) ClearTemps() {
	for {
		files, err := ioutil.ReadDir("temp")
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range files {
			file, err := os.Stat("temp/" + f.Name())

			if err != nil {
				log.Println(err)
			}

			now := time.Now().Add(-300 * time.Second)

			if file.ModTime().Unix() <= now.Unix() {
				modifiedtime := file.ModTime().Format("2006-01-02 15:04:05")

				log.Println("Cleaning: " + f.Name() + " -> " + modifiedtime)
				os.Remove("temp/" + f.Name())
			}

		}

		time.Sleep(15 * time.Second)
	}
}
