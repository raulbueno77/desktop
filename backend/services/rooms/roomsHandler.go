package rooms

import (
	"bufio"
	"bytes"
	"cehotel/backend/models"
	bookingpb "cehotel/backend/proto/booking"
	roompb "cehotel/backend/proto/room"
	"context"
	"encoding/base64"
	"encoding/json"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type RoomClient struct {
	service roompb.RoomServicesClient
	serviceAttr roompb.AttributeServicesClient
	serviceBooking bookingpb.BookingServicesClient
}

func NewRoomClient (cc *grpc.ClientConn) *RoomClient {
	service := roompb.NewRoomServicesClient(cc)
	serviceAttr := roompb.NewAttributeServicesClient(cc)
	serviceBooking := bookingpb.NewBookingServicesClient(cc)

	return &RoomClient{
		service: service,
		serviceAttr: serviceAttr,
		serviceBooking: serviceBooking,
	}
}

func (r *RoomClient) CreateRoomType (roomTypeMap map[string]interface{}, deleteImages string, token string) (models.RoomType, error) {
	log.Println("Create or Update Room Type...")
	ctx, cancel := context.WithTimeout(context.Background(), 360*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	roomType := models.RoomType{}
	data, err := json.Marshal(roomTypeMap)

	if err != nil {
		return roomType, err
	}

	if err := json.Unmarshal(data, &roomType); err != nil {
		return roomType, err
	}

	if deleteImages != "" {
		imagesToDelete := strings.Split(deleteImages, "##")

		for _, img := range imagesToDelete {
			r.service.DeletePicture(ctx, &roompb.DeletePictureRequest{
				PictureId:            0,
				Filename:             img,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			})


			os.Remove("media/" + img)
		}

	}

	//save the room type
	if roomType.Id == 0 {
		res, err := r.service.CreateRoomType(ctx, &roompb.CreateRoomTypeRequest{
			RoomType:             roomType.Parse(),
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return roomType, err
		}

		roomType.Id = res.GetRoomType().GetId()
	}else {
		_, err := r.service.UpdateRoomType(ctx, &roompb.UpdateRoomTypeRequest{
			RoomType:             roomType.Parse(),
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return roomType, err
		}
	}

	//save the pictures
	pictures := roomType.Pictures

	for _, picture := range pictures {
		if picture.New {
			r.UploadImageToServer(picture, roomType.Id,token)
		}else {
			if picture.Main {
				_, err := r.service.PutPictureMain(ctx, &roompb.PutPictureMainRequest{
					PictureId:            0,
					Filename:             picture.Id,
					XXX_NoUnkeyedLiteral: struct{}{},
					XXX_unrecognized:     nil,
					XXX_sizecache:        0,
				})

				if err != nil {
					log.Println(err)
				}
			}
		}
	}

	return roomType, nil
}

func (r *RoomClient) DeleteImageFromServer (filename string, token string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	_, err := r.service.DeletePicture(ctx, &roompb.DeletePictureRequest{
		PictureId:            0,
		Filename:             filename,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return err
	}

	return nil
}

func (r *RoomClient) UploadImageToServer (picture *models.RoomPictures, idRoomType int32, token string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	file, err := os.Open("media/" + picture.Id)

	if err != nil {
		return err
	}
	defer file.Close()

	stream, err := r.service.UploadPictureRoom(ctx)

	if err != nil {
		return err
	}

	req := &roompb.UploadPictureRoomRequest{
		Data:                 &roompb.UploadPictureRoomRequest_PictureData{PictureData: &roompb.PictureRoomData{
			RoomTypeId:           idRoomType,
			Filename:             picture.Id,
			AltText:              "",
			Main:                 picture.Main,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		}},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
	log.Println("Hello")
	err = stream.Send(req)
	if err != nil {
		return err
	}

	reader := bufio.NewReader(file)
	buffer := make([]byte, 1024)

	for {
		n, err := reader.Read(buffer)
		log.Println(n)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Println(err)
			log.Fatal("cannot read chunk to buffer: ", err)
		}

		req := &roompb.UploadPictureRoomRequest{
			Data:                 &roompb.UploadPictureRoomRequest_Content{Content: buffer[:n]},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		}

		err = stream.Send(req)
		if err != nil {
			log.Println(err)
			return err
		}
	}

	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Println(err)
		return err
	}

	log.Println(res)

	//change img
	os.Rename("media/" + picture.Id, "media/" + res.GetFilename())

	if picture.Main {
		_, err := r.service.PutPictureMain(ctx, &roompb.PutPictureMainRequest{
			PictureId:            0,
			Filename:             res.GetFilename(),
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			log.Println(err)
		}
	}

	return nil
}

func (r *RoomClient) SaveImage(fileJson string) (string, error) {
	os.Mkdir("media", os.ModePerm)
	file := &models.File{}
	if err := json.Unmarshal([]byte(fileJson), &file); err != nil {
		return "", err
	}
	fileToSave, err := os.Create("media/" + file.Name)

	if err != nil {
		return "", err
	}

	defer fileToSave.Close()
	fileData := bytes.Buffer{}

	fileData.Write(file.Data)

	fileData.WriteTo(fileToSave)

	return file.Name, nil
}

func (r *RoomClient) DeleteImageTemp(fileName string) error {
	err := os.Remove("media/" + fileName)

	if err != nil {
		return err
	}

	return nil
}

func (r *RoomClient) GetRoomType (id int32, token string) (models.RoomType, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)


	res, err := r.service.GetRoomType(ctx, &roompb.GetRoomTypeRequest{
		RoomTypeId:           id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return models.RoomType{}, err
	}

	roomType := models.RoomType{}

	pictures := []*models.RoomPictures{}

	for _, pic := range res.Pictures {
		f, err := os.Open("media/" + pic.Filename)

		if err != nil {
			filename, _ := r.DownloadPicture(pic.Filename, token)

			f, _ = os.Open("media/" + filename)
		}

		// Read entire JPG into byte slice.
		reader := bufio.NewReader(f)
		content, _ := ioutil.ReadAll(reader)

		fileSplit := strings.Split(pic.Filename, ".")
		// Encode as base64.
		encoded := "data:image/" + fileSplit[len(fileSplit) - 1] + ";base64," + base64.StdEncoding.EncodeToString(content)
		pictures = append(pictures, &models.RoomPictures{
			Id:   pic.Filename,
			Main: pic.Main,
			Src:  encoded,
			New:  false,
		})
	}

	roomType.Hydrate(res.RoomType, pictures)

	return roomType, nil
}

func (r *RoomClient) FindRoomTypes(hotelId int32, filter string, token string) ([]models.RoomTypeList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := r.service.FindRoomTypes(ctx, &roompb.FindRoomTypesRequest{
		Filter:               strings.TrimSpace(filter),
		Sort:                 "",
		SortDirection:        "",
		HotelId:              hotelId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	roomTypes := []models.RoomTypeList{}

	for _, rtype := range res.GetRoomTypes() {
		roomTypes = append(roomTypes, models.RoomTypeList{
			Id:          rtype.GetId(),
			Name:        rtype.GetName(),
			MainPicture: func () *string {
				if rtype.MainPicture != "" {
					f, err := os.Open("media/" + rtype.MainPicture)

					if err != nil {
						filename, _ := r.DownloadPicture(rtype.MainPicture, token)

						f, _ = os.Open("media/" + filename)
					}

					// Read entire JPG into byte slice.
					reader := bufio.NewReader(f)
					content, _ := ioutil.ReadAll(reader)

					fileSplit := strings.Split(rtype.MainPicture, ".")
					// Encode as base64.
					encoded := "data:image/" + fileSplit[len(fileSplit) - 1] + ";base64," + base64.StdEncoding.EncodeToString(content)
					return &encoded
				}

				return nil
			}(),
		})
	}

	return roomTypes, nil
}

func (r *RoomClient) DownloadPicture (filename string, token string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	req := &roompb.GetPictureRequest{
		Id:                   0,
		Filename:             filename,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	stream, err := r.service.GetPicture(ctx, req)

	if err != nil {
		return "", err
	}

	//res, err := stream.Recv()

	//fileinfo := res.GetFileData()

	fileData := bytes.Buffer{}
	filesize := 0

	for {
		res, err := stream.Recv()

		if err == io.EOF {
			break
		}

		if err != nil {
			return "", err
		}

		chunk := res.GetContent()
		filesize += len(chunk)

		_, err = fileData.Write(chunk)

		if err != nil {
			return "", err
		}
	}

	if filesize > 0 {
		fileToSave, err := os.Create("media/" + filename)

		if err != nil {
			return "", err
		}
		defer fileToSave.Close()

		fileData.WriteTo(fileToSave)
	}

	return filename, nil
}

func (r *RoomClient) BookingCalendar(start string, end string, hotelId string) ([]*models.Event, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	req := &roompb.BookingsCalendarRequest{
		Start:                start,
		End:                  end,
		HotelId:              hotelId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	res, err := r.service.BookingsCalendar(ctx, req)

	if err != nil {
		return nil, err
	}

	events := []*models.Event{}

	for _, r := range res.GetRooms() {
		events = append(events, &models.Event{
			Title: r.GetTitle(),
			Start: r.GetStart(),
			End:   r.GetEnd(),
			Color: func () string {
				if !r.GetFree() {
					if r.CheckIn != "" {
						return "red"
					}else {
						return "orange"
					}
				}

				return "green"
			}(),
		})
	}

	return events, nil
}

//attributes
func (r *RoomClient) ListAttributes (token string) ([]*models.Attribute, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := r.serviceAttr.GetAllAttributes(ctx, &roompb.GetAllAttributesRequest{
		Filter:               "",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	attributes := []*models.Attribute{}

	for _, attr := range res.GetAttribute() {
		attributes = append(attributes, &models.Attribute{
			Id:   attr.GetId(),
			Name: attr.GetName(),
		})
	}

	return attributes, nil
}

func (r *RoomClient) CreateAttribute(attributeMap map[string]interface{}, token string) (*models.Attribute, error) {
	log.Println("Create or Update Room Attribute...")
	ctx, cancel := context.WithTimeout(context.Background(), 360*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	attribute := models.Attribute{}
	data, err := json.Marshal(attributeMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data, &attribute); err != nil {
		return nil, err
	}

	if attribute.Id == 0 {
		res, err := r.serviceAttr.CreateAttribute(ctx, &roompb.CreateAttributeRequest{
			Atribute:             &roompb.Attribute{
				Id:                   attribute.Id,
				Name:                 attribute.Name,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return nil, err
		}

		attribute.Id = res.GetAttribute().GetId()
	}else {
		_, err := r.serviceAttr.UpdateAttribute(ctx, &roompb.UpdateAttributeRequest{
			Atribute:             &roompb.Attribute{
				Id:                   attribute.Id,
				Name:                 attribute.Name,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return nil, err
		}
	}

	return &attribute, nil
}

func (r *RoomClient) GetAttribute(id int32, token string) (*models.Attribute, error) {
	log.Println("Get Room Attribute...")
	ctx, cancel := context.WithTimeout(context.Background(), 360*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	
	attribute := models.Attribute{}
	
	res, err := r.serviceAttr.GetAttribute(ctx, &roompb.GetAttributeRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})
	
	if err != nil {
		return nil, err
	}
	
	attribute.Id = res.GetAttribute().GetId()
	attribute.Name = res.GetAttribute().GetName()
	
	return &attribute, nil
}

func (r *RoomClient) DeleteAttribute(id int32, token string) (string, error) {
	log.Println("Get Room Attribute...")
	ctx, cancel := context.WithTimeout(context.Background(), 360*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	_, err := r.serviceAttr.DeleteAttribute(ctx, &roompb.DeleteAttributeRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return "", err
	}

	return "Operation OK!!!!", nil
}

//rooms
func (r *RoomClient) CreateRoom(roomMap map[string]interface{}, token string) (*models.Room, error) {
	log.Println("Create or Update Room...")
	ctx, cancel := context.WithTimeout(context.Background(), 360*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	room := models.Room{}
	data, err := json.Marshal(roomMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data, &room); err != nil {
		return nil, err
	}

	if room.Id == 0 {
		res, err := r.service.CreateRoom(ctx, &roompb.CreateRoomRequest{
			Room:                 room.Parse(),
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return nil, err
		}

		room.Id = res.GetRoom().GetId()
	}else {
		_, err := r.service.UpdateRoom(ctx, &roompb.UpdateRoomRequest{
			Room:                 room.Parse(),
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return nil, err
		}
	}

	return &room, nil
}

func (r *RoomClient) GetRoom(id int32, token string) (*models.Room, error) {
	log.Println("Get Room...")
	ctx, cancel := context.WithTimeout(context.Background(), 360*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	req := &roompb.GetRoomRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	res, err := r.service.GetRoom(ctx, req)

	if err != nil {
		return nil, err
	}

	room := models.Room{}

	room.Hydrate(res.GetRoom())


	return &room, nil
}

func (r *RoomClient) ListRooms (idHotel int32, ftr string, page int32, sort string, sortDirection string,token string) (*models.RoomList, error) {
	log.Println("List Rooms...")
	ctx, cancel := context.WithTimeout(context.Background(), 360*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	req := &roompb.FindRoomsRequest{
		IdHotel:              idHotel,
		Ftr:                  strings.TrimSpace(ftr),
		Page:                 page,
		Sort:                 sort,
		SortDirection:        sortDirection,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	rooms := []models.RoomView{}

	res, err := r.service.FindRooms(ctx, req)

	if err != nil {
		return nil, err
	}

	for _, room := range res.Room  {
		rooms = append(rooms, models.RoomView{
			Id:       room.GetId(),
			Name:     room.GetName(),
			Floor:    room.GetFloor(),
			Type:     room.GetRoomType().GetName(),
			Disabled: room.GetDisabled(),
		})
	}

	pages := []models.Select{}

	for i := 1; i <= int(res.GetTotalPages()); i++ {
		pages = append(pages, models.Select{
			Text:  strconv.Itoa(i) + " de " + strconv.Itoa(int(res.GetTotalPages())),
			Value: int32(i),
		})
	}

	return &models.RoomList{
		Rooms:       rooms,
		TotalPages:  res.TotalPages,
		SelectPages: pages,
	}, nil

}

func (r *RoomClient) DeleteRoom(id int32, token string) (string, error) {
	log.Println("Delete Room...")
	ctx, cancel := context.WithTimeout(context.Background(), 360*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	req := &roompb.DeleteRoomRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	_, err := r.service.DeleteRoom(ctx, req)

	if err != nil {
		return "", err
	}

	return "Operation OK!!!", nil
}

func (r *RoomClient) GetTypesSelect (hotelId int32, token string) (*[]models.Select, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := r.service.FindRoomTypes(ctx, &roompb.FindRoomTypesRequest{
		Filter:               "",
		Sort:                 "name",
		SortDirection:        "asc",
		HotelId:              hotelId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	types := []models.Select{}

	for _, rtype := range res.RoomTypes {
		types = append(types, models.Select{
			Text:  rtype.Name,
			Value: rtype.Id,
		})
	}

	return &types, nil
}

func (r *RoomClient) GetAttributesSelect (token string) (*[]models.Select, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := r.serviceAttr.GetAllAttributes(ctx, &roompb.GetAllAttributesRequest{
		Filter:               "",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	attributes := []models.Select{}

	for _, attr := range res.GetAttribute() {
		attributes = append(attributes, models.Select{
			Text:  attr.Name,
			Value: attr.Id,
		})
	}

	return &attributes, nil
}

func (r *RoomClient) GetPrice (roomName string, begin string, end string, token string) (*float32, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := r.serviceBooking.GetPrice(ctx, &bookingpb.GetPriceRequest{
		RoomId:               0,
		DateBegin:            begin,
		DateEnd:              end,
		RoomName:             roomName,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	price := res.GetPrice()

	return &price, nil
}