package saleInvoices

import (
	"cehotel/backend/models"
	saleinvoicepb "cehotel/backend/proto/sale_invoice"
	"context"
	"encoding/json"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"log"
	"strconv"
	"strings"
	"time"
)

type SaleInvoicesClient struct {
	service saleinvoicepb.SaleInvoiceServicesClient
}

func NewSaleInvoiceClient(cc *grpc.ClientConn) *SaleInvoicesClient {
	return &SaleInvoicesClient{service: saleinvoicepb.NewSaleInvoiceServicesClient(cc)}
}

func (s *SaleInvoicesClient) CreateSaleInvoice (invoiceMap map[string]interface{}, detailsMap []interface{}, token string) (*models.SaleInvoice, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	invoice := models.SaleInvoice{}
	log.Println(invoiceMap)
	data, err := json.Marshal(invoiceMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data, &invoice); err != nil {
		log.Println(err)
		return nil, err
	}

	log.Println(&invoice)
	details := []models.SaleInvoceDetail{}

	data2, err := json.Marshal(detailsMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data2, &details); err != nil {
		return nil, err
	}

	invoiceGrpc := &saleinvoicepb.SaleInvoice{
		Id:                   invoice.Id,
		ClientId:             invoice.ClientId,
		Document:             invoice.Document,
		FirstName:            invoice.FirstName,
		LastName:             invoice.LastName,
		SecondLastName:       invoice.SecondLastName,
		Address:              invoice.Address,
		PostalCode:           invoice.PostalCode,
		Country:              invoice.Country,
		Region:               invoice.Region,
		City:                 invoice.City,
		HomePhone:            invoice.HomePhone,
		CellPhone:            invoice.CellPhone,
		Email:                invoice.Email,
		Observations:         invoice.Observations,
		Date:                 invoice.Date,
		Total:                invoice.Total,
		SerialInvoiceId:      invoice.SerialInvoiceId,
		Number:               func(value string) int32 {
			if strings.TrimSpace(value) == "" {
				return 0
			}

			number, err := strconv.Atoi(value)

			if err != nil {
				return 0
			}

			return int32(number)
		} (invoice.Number),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	detailsGrpc := []*saleinvoicepb.SaleInvoiceDetail{}

	for _, det := range details {
		detailsGrpc = append(detailsGrpc, &saleinvoicepb.SaleInvoiceDetail{
			Id:                   det.Id,
			SaleInvoiceId:        det.SaleInvoiceId,
			Reference:            det.Reference,
			Description:          det.Description,
			Units:                det.Units,
			Tax:                  det.Tax,
			Price:                det.Price,
			Total:                det.Total,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})
	}

	res, err := s.service.CreateSaleInvoice(ctx, &saleinvoicepb.CreateSaleInvoiceRequest{
		Invoice:              invoiceGrpc,
		Details:              detailsGrpc,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	invoice.Number =  strconv.Itoa(int(res.GetInvoice().GetNumber()))

	detailsReturn := []models.SaleInvoceDetail{}

	for _, det := range details {
		det.SaleInvoiceId = res.GetInvoice().Id
		detailsReturn = append(detailsReturn, det)
	}

	invoice.Id = res.GetInvoice().GetId()
	invoice.Details = &detailsReturn
	return &invoice, nil
}

func (s *SaleInvoicesClient) UpdateSaleInvoice (invoiceMap map[string]interface{}, detailsMap []interface{}, token string) (*models.SaleInvoice, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	invoice := models.SaleInvoice{}
	data, err := json.Marshal(invoiceMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data, &invoice); err != nil {
		return nil, err
	}

	details := []models.SaleInvoceDetail{}

	data2, err := json.Marshal(detailsMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data2, &details); err != nil {
		return nil, err
	}

	invoiceGrpc := &saleinvoicepb.SaleInvoice{
		Id:                   invoice.Id,
		ClientId:             invoice.ClientId,
		Document:             invoice.Document,
		FirstName:            invoice.FirstName,
		LastName:             invoice.LastName,
		SecondLastName:       invoice.SecondLastName,
		Address:              invoice.Address,
		PostalCode:           invoice.PostalCode,
		Country:              invoice.Country,
		Region:               invoice.Region,
		City:                 invoice.City,
		HomePhone:            invoice.HomePhone,
		CellPhone:            invoice.CellPhone,
		Email:                invoice.Email,
		Observations:         invoice.Observations,
		Date:                 invoice.Date,
		Total:                invoice.Total,
		SerialInvoiceId:      invoice.SerialInvoiceId,
		Number:               func(value string) int32 {
			if strings.TrimSpace(value) == "" {
				return 0
			}

			number, err := strconv.Atoi(value)

			if err != nil {
				return 0
			}

			return int32(number)
		} (invoice.Number),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	detailsGrpc := []*saleinvoicepb.SaleInvoiceDetail{}

	for _, det := range details {
		detailsGrpc = append(detailsGrpc, &saleinvoicepb.SaleInvoiceDetail{
			Id:                   det.Id,
			SaleInvoiceId:        det.SaleInvoiceId,
			Reference:            det.Reference,
			Description:          det.Description,
			Units:                det.Units,
			Tax:                  det.Tax,
			Price:                det.Price,
			Total:                det.Total,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})
	}

	res, err := s.service.UpdateSaleInvoice(ctx, &saleinvoicepb.UpdateSaleInvoiceRequest{
		Invoice:              invoiceGrpc,
		Details:              detailsGrpc,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	detailsReturn := []models.SaleInvoceDetail{}

	for _, det := range details {
		det.SaleInvoiceId = res.GetInvoice().Id
		detailsReturn = append(detailsReturn, det)
	}

	invoice.Id = res.GetInvoice().GetId()
	invoice.Details = &detailsReturn
	return &invoice, nil
}

func (s *SaleInvoicesClient) GetSaleInvoice (id int32, token string) (*models.SaleInvoice, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := s.service.GetSaleInvoice(ctx, &saleinvoicepb.GetSaleInvoiceRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	invoiceGrpc := res.GetInvoice()
	detailsGrpc := res.GetDetails()

	details := []models.SaleInvoceDetail{}

	for _, det := range detailsGrpc{
		details = append(details, models.SaleInvoceDetail{
			Id:            det.GetId(),
			SaleInvoiceId: det.GetSaleInvoiceId(),
			Reference:     det.GetReference(),
			Description:   det.GetDescription(),
			Units:         det.GetUnits(),
			Tax:           det.GetTax(),
			Price:         det.GetPrice(),
			Total:         det.GetTotal(),
		})
	}

	invoice := models.SaleInvoice{
		Id:              invoiceGrpc.GetId(),
		ClientId:        invoiceGrpc.GetClientId(),
		FirstName:       invoiceGrpc.GetFirstName(),
		LastName:        invoiceGrpc.GetLastName(),
		SecondLastName:  invoiceGrpc.GetSecondLastName(),
		Document:        invoiceGrpc.GetDocument(),
		Address:         invoiceGrpc.GetAddress(),
		PostalCode:      invoiceGrpc.GetPostalCode(),
		Country:         invoiceGrpc.GetCountry(),
		Region:          invoiceGrpc.GetRegion(),
		City:            invoiceGrpc.GetCity(),
		HomePhone:       invoiceGrpc.GetHomePhone(),
		CellPhone:       invoiceGrpc.GetCellPhone(),
		Email:           invoiceGrpc.GetEmail(),
		Observations:    invoiceGrpc.GetObservations(),
		Date:            invoiceGrpc.GetDate(),
		Total:           invoiceGrpc.GetTotal(),
		Number:          strconv.Itoa(int(invoiceGrpc.GetNumber())),
		SerialInvoiceId: invoiceGrpc.GetSerialInvoiceId(),
		Details:         &details,
	}

	return &invoice, nil
}

func (s *SaleInvoicesClient) DeleteSaleInvoice (id int32, token string) (*string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := s.service.DeleteSaleInvoice(ctx, &saleinvoicepb.DeleteSaleInvoiceRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	message := res.GetMessage()

	return &message, nil
}

func (s *SaleInvoicesClient) FindSaleInvoices (begin string, end string, clientId int32, filter string, page int32, sort string, sortDirection string, serial_id int32, token string) (*models.SaleInvoicesList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	req := &saleinvoicepb.FindSaleInvoicesRequest{
		Begin:                begin,
		End:                  end,
		ClientId:             clientId,
		Filter:               filter,
		Page:                 page,
		Sort:                 sort,
		SortDirection:        sortDirection,
		SerialId: 			  serial_id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	res, err := s.service.FindSaleInvoices(ctx, req)

	if err != nil {
		return nil, err
	}

	invoices := []models.SaleInvoiceListView{}

	for _, inv := range res.GetInvoices() {
		invoices = append(invoices, models.SaleInvoiceListView{
			Id:       inv.GetId(),
			Date:     inv.GetDate(),
			Document: inv.GetDocument(),
			Name:     strings.TrimSpace(inv.GetFirstName() + " " + inv.GetLastName() + " " + inv.GetSecondLastName()),
			Total:    inv.GetTotal(),
		})
	}

	pages := []models.Select{}

	for i := 1; i <= int(res.GetTotalPages()); i++ {
		pages = append(pages, models.Select{
			Text:  strconv.Itoa(i) + " de " + strconv.Itoa(int(res.GetTotalPages())),
			Value: int32(i),
		})
	}

	return &models.SaleInvoicesList{
		SaleInvoices: invoices,
		TotalPages:   res.GetTotalPages(),
		SelectPages:  pages,
	}, nil
}

func (s *SaleInvoicesClient) CreateSerialInvoice (serialMap map[string]interface{}, token string) (*models.SerialInvoice, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	serial := models.SerialInvoice{}
	data, err := json.Marshal(serialMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data, &serial); err != nil {
		return nil, err
	}

	res, err := s.service.CreateSerialInvoice(ctx, &saleinvoicepb.CreateSerialInvoiceRequest{
		Serial:               &saleinvoicepb.SerialInvoice{
			Id:                   serial.Id,
			Serial:               serial.Serial,
			Record:               func () bool {
				if serial.Record == "true" {
					return true
				}

				return false
			}(),
			Name:                 serial.Name,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	serial.Id = res.GetSerial().GetId()

	return &serial, nil
}

func (s *SaleInvoicesClient) UpdateSerialInvoice (serialMap map[string]interface{}, token string) (*models.SerialInvoice, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	serial := models.SerialInvoice{}
	data, err := json.Marshal(serialMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data, &serial); err != nil {
		return nil, err
	}

	_, err = s.service.UpdateSerialInvoice(ctx, &saleinvoicepb.UpdateSerialInvoiceRequest{
		Serial:               &saleinvoicepb.SerialInvoice{
			Id:                   serial.Id,
			Serial:               serial.Serial,
			Record:               func () bool {
				if serial.Record == "true" {
					return true
				}

				return false
			}(),
			Name:                 serial.Name,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	return &serial, nil
}

func (s *SaleInvoicesClient) GetSerialInvoice (id int32, token string) (*models.SerialInvoice, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := s.service.GetSerialInvoice(ctx, &saleinvoicepb.GetSerialInvoiceRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	return &models.SerialInvoice{
		Id:     res.GetSerial().GetId(),
		Name:   res.GetSerial().GetName(),
		Serial: res.GetSerial().GetSerial(),
		Record: func () string {
			if res.GetSerial().GetRecord() {
				return "true"
			}

			return "false"
		}(),
	}, nil
}

func (s *SaleInvoicesClient) DeleteSerialInvoice (id int32, token string) (*string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := s.service.DeleteSerialInvoice(ctx, &saleinvoicepb.DeleteSerialInvoiceRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}
	
	message := res.GetMessage()
	
	return &message, nil
}

func (s *SaleInvoicesClient) FindSerialInvoices (token string) ([]*models.SerialInvoice, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	
	res, err := s.service.FindSerialInvoices(ctx, &saleinvoicepb.FindSerialInvoicesRequest{
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})
	
	if err != nil {
		return nil, err
	}
	
	serials := []*models.SerialInvoice{}
	
	for _, serial := range res.GetSerial() {
		serials = append(serials, &models.SerialInvoice{
			Id:     serial.GetId(),
			Name:   serial.GetName(),
			Serial: serial.GetSerial(),
			Record: func () string {
				if serial.GetRecord() {
					return "true"
				}

				return "false"
			}(),
		})
	}

	return serials, nil
}