package hotels

import (
	"bufio"
	"bytes"
	"cehotel/backend/models"
	hotelpb "cehotel/backend/proto/hotel"
	"context"
	"encoding/base64"
	"encoding/json"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type HotelClient struct {
	service hotelpb.HotelServicesClient
}

func NewHotelClient(cc *grpc.ClientConn) *HotelClient {
	service := hotelpb.NewHotelServicesClient(cc)
	
	return &HotelClient{service: service}
}

func (h *HotelClient) GetHotelsSelect (token string) (*[]models.Select, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := h.service.FindHotel(ctx, &hotelpb.FindHotelRequest{
		Filter:               "",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	hotels := []models.Select{}

	for _, hotel := range res.Hotels {
		hotels = append(hotels, models.Select{
			Text:  hotel.Name,
			Value: hotel.Id,
		})
	}

	return &hotels, nil
}

func (h *HotelClient) GetHotelsByUser (id int32, token string) ([]*models.Hotel, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := h.service.FindHotel(ctx, &hotelpb.FindHotelRequest{
		Filter:               "",
		UserId:               id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	hotels := []*models.Hotel{}

	for _, hotel := range res.GetHotels() {
		hot := models.Hotel{}

		logo := &models.LogoHotel{
			Id: hotel.Logo,
			Src: "",
		}
		hot.Hydrate(hotel, logo)

		hotels = append(hotels, &hot)
	}

	return hotels, nil
}

func (h *HotelClient) CreateHotel(hotelMap map[string]interface{}, token string) (models.Hotel, error) {
	log.Println("Create or Update Hotel...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	hotel := models.Hotel{}
	data, err := json.Marshal(hotelMap)

	if err != nil {
		return hotel, err
	}

	if err := json.Unmarshal(data, &hotel); err != nil {
		return hotel, err
	}

	delete_logo := hotel.DeleteLogo
	
	hotelGrpc := hotel.Parse()

	if hotelGrpc.Id == 0 {
		res, err := h.service.CreateHotel(ctx, &hotelpb.CreateHotelRequest{
			Hotel:                hotelGrpc,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return hotel, err
		}

		hotel.Id = res.GetHotel().GetId()
	}else {
		if delete_logo {
			hotelGrpc.Logo = ""

			h.service.DeleteLogo(ctx, &hotelpb.DeleteLogoRequest{
				HotelId:              hotelGrpc.Id,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			})
		}

		res, err := h.service.GetHotel(ctx, &hotelpb.GetHotelRequest{
			Id:                   hotelGrpc.Id,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return hotel, err
		}

		if res.GetHotel().GetLogo() != "" {
			hotelGrpc.Logo = res.GetHotel().GetLogo()
		}
		
		_, err = h.service.UpdateHotel(ctx, &hotelpb.UpdateHotelRequest{
			Hotel:                hotelGrpc,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return hotel, err
		}
	}

	//upload logo if it's necesary
	logo := hotel.Logo

	if logo.New {
		h.UploadImageToServer(logo, hotel.Id, token)
	}

	return hotel, nil
}

func (h *HotelClient) GetHotel (id string, token string) (models.Hotel, error) {
	log.Println("Get Hotel...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	hotel := models.Hotel{}

	idHotel, err := strconv.Atoi(id)

	if err != nil {
		return models.Hotel{}, err
	}


	res, err := h.service.GetHotel(ctx, &hotelpb.GetHotelRequest{
		Id:                   int32(idHotel),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return hotel, err
	}

	logo := &models.LogoHotel{
		Id: res.GetHotel().GetLogo(),
		Src: "",
		New: false,
	}

	hotel.Hydrate(res.GetHotel(), logo)

	if res.GetHotel().GetLogo() != "" {
		f, err := os.Open("media/" + res.GetHotel().GetLogo())

		if err != nil {
			filename, _ := h.DownloadLogo(res.GetHotel().GetId(), res.GetHotel().GetLogo(), token)

			f, _ = os.Open("media/" + filename)
		}
		// Read entire JPG into byte slice.
		reader := bufio.NewReader(f)
		content, _ := ioutil.ReadAll(reader)

		fileSplit := strings.Split(res.GetHotel().GetLogo(), ".")
		// Encode as base64.
		encoded := "data:image/" + fileSplit[len(fileSplit) - 1] + ";base64," + base64.StdEncoding.EncodeToString(content)

		logo.Src = encoded

		hotel.Logo = logo
	}

	return hotel, nil
}

func (h *HotelClient) DownloadLogo (hotelId int32, filename string, token string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)


	req := &hotelpb.GetLogoRequest{
		HotelId:              hotelId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}


	stream, err := h.service.GetLogo(ctx, req)

	if err != nil {
		return "", err
	}

	//res, err := stream.Recv()

	//fileinfo := res.GetFileData()

	fileData := bytes.Buffer{}
	filesize := 0

	for {
		res, err := stream.Recv()

		if err == io.EOF {
			break
		}

		if err != nil {
			return "", err
		}

		chunk := res.GetContent()
		filesize += len(chunk)

		_, err = fileData.Write(chunk)

		if err != nil {
			return "", err
		}
	}

	if filesize > 0 {
		fileToSave, err := os.Create("media/" + filename)

		if err != nil {
			return "", err
		}
		defer fileToSave.Close()

		fileData.WriteTo(fileToSave)
	}

	return filename, nil
}

func (h *HotelClient) FindHotels (ftr string,token string) ([]*models.Hotel, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := h.service.FindHotel(ctx, &hotelpb.FindHotelRequest{
		Filter:               strings.TrimSpace(ftr),
		UserId:               0,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	hotels := []*models.Hotel{}

	for _, hotel := range res.GetHotels() {
		hot := models.Hotel{}

		logo := &models.LogoHotel{
			Id: hotel.Logo,
			Src: "",
		}

		hot.Hydrate(hotel, logo)

		hotels = append(hotels, &hot)
	}

	return hotels, nil
}

func (h *HotelClient) DeleteHotel (id int32, token string) error {
	log.Println("Delete Hotel...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	_, err := h.service.DeleteHotel(ctx, &hotelpb.DeleteHotelRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return err
	}

	return nil
}

func (r *HotelClient) SaveImage(fileJson string) (string, error) {
	os.Mkdir("media", os.ModePerm)
	file := &models.File{}
	if err := json.Unmarshal([]byte(fileJson), &file); err != nil {
		return "", err
	}
	fileToSave, err := os.Create("media/" + file.Name)

	if err != nil {
		return "", err
	}

	defer fileToSave.Close()
	fileData := bytes.Buffer{}

	fileData.Write(file.Data)

	fileData.WriteTo(fileToSave)

	return file.Name, nil
}

func (r *HotelClient) DeleteImageTemp(fileName string) error {
	err := os.Remove("media/" + fileName)

	if err != nil {
		return err
	}

	return nil
}

func (r *HotelClient) UploadImageToServer (picture *models.LogoHotel, idHotel int32, token string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	file, err := os.Open("media/" + picture.Id)

	if err != nil {
		return err
	}
	defer file.Close()

	stream, err := r.service.UploadLogo(ctx)

	if err != nil {
		return err
	}

	req := &hotelpb.UploadLogoHotelRequest{
		Data:                 &hotelpb.UploadLogoHotelRequest_PictureData{PictureData: &hotelpb.LogoHotelData{
			HotelId:              idHotel,
			Filename:             picture.Id,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		}},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	err = stream.Send(req)
	if err != nil {
		return err
	}

	reader := bufio.NewReader(file)
	buffer := make([]byte, 1024)

	for {
		n, err := reader.Read(buffer)
		log.Println(n)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Println(err)
			log.Fatal("cannot read chunk to buffer: ", err)
		}

		req := &hotelpb.UploadLogoHotelRequest{
			Data:                 &hotelpb.UploadLogoHotelRequest_Content{Content: buffer[:n]},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		}

		err = stream.Send(req)
		if err != nil {
			log.Println(err)
			return err
		}
	}

	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Println(err)
		return err
	}

	//change img
	os.Rename("media/" + picture.Id, "media/" + res.GetFilename())

	return nil
}
