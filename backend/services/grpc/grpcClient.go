package grpc

import "google.golang.org/grpc"

type GrpcClient struct {
	Host		string
	Port		string
	Connection 	bool
}

func NewGrpcClient(host string, port string) *GrpcClient {
	return &GrpcClient{
		Host: host,
		Port: port,
		Connection: false,
	}
}

func (g *GrpcClient) CreateCC() (*grpc.ClientConn, error) {
	cc, err := grpc.Dial(g.Host + ":" + g.Port, grpc.WithInsecure())

	if err != nil {
		return nil, err
	}

	return cc, nil
}
