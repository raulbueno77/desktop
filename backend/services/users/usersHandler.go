package users

import (
	"cehotel/backend/models"
	hotelpb "cehotel/backend/proto/hotel"
	userpb "cehotel/backend/proto/user"
	userGrouppb "cehotel/backend/proto/userGroup"
	"context"
	"encoding/json"
	"log"
	"strconv"
	"strings"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type UserClient struct {
	service    userpb.UserServicesClient
	serviceGrp userGrouppb.UserGroupServicesClient
	serviceHot hotelpb.HotelServicesClient
}

func NewUserClient(cc *grpc.ClientConn) *UserClient {
	service := userpb.NewUserServicesClient(cc)
	serviceGrp := userGrouppb.NewUserGroupServicesClient(cc)
	serviceHot := hotelpb.NewHotelServicesClient(cc)

	return &UserClient{
		service:    service,
		serviceGrp: serviceGrp,
		serviceHot: serviceHot,
	}
}

func (cli *UserClient) TestConnection() bool {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	req := &userpb.TestConnectionRequest{
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	_, err := cli.service.TestConnection(ctx, req)

	if err != nil {
		return false
	}

	return true
}

func (cli *UserClient) Login(username string, password string) (*models.LoginResponse, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	req := &userpb.LoginRequest{
		Username: username,
		Password: password,
	}

	res, err := cli.service.Login(ctx, req)
	if err != nil {
		log.Printf("Error username / password: %v", err)
		return nil, err
	}

	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", res.GetToken())
	resMe, err := cli.service.Me(ctx, &userpb.MeRequest{
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		log.Printf("Error getting user: %v", err)
		return nil, err
	}

	resGrp, err := cli.serviceGrp.GetUserGroup(ctx, &userGrouppb.GetUserGroupRequest{
		Id:                   resMe.GetUser().GetUserGroupId(),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		log.Printf("Error getting group: %v", err)
		return nil, err
	}

	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", res.GetToken())

	return &models.LoginResponse{
		Token:    res.GetToken(),
		UserId:   resMe.GetUser().GetId(),
		Username: resMe.GetUser().GetUsername(),
		Fullname: strings.TrimSpace(resMe.GetUser().GetFirstname() + " " + resMe.GetUser().GetLastname()),
		Role:     resGrp.GetUserGroup().GetName(),
		Hotels:   func () []*models.HotelShort {
			hotels := []*models.HotelShort{}
			log.Printf("Hoteles del usuario: %v", len(resMe.GetUser().GetHotels()))
			if len(resMe.GetUser().GetHotels()) > 0 && resGrp.GetUserGroup().GetName() != "Admin" {
				for _, hot := range resMe.GetUser().GetHotels() {
					hotel, _ := cli.serviceHot.GetHotel(ctx, &hotelpb.GetHotelRequest{
						Id:                   hot,
						XXX_NoUnkeyedLiteral: struct{}{},
						XXX_unrecognized:     nil,
						XXX_sizecache:        0,
					})

					hotels = append(hotels, &models.HotelShort{
						Id:   hotel.GetHotel().GetId(),
						Name: hotel.GetHotel().GetName(),
					})
				}
			} else  if resGrp.GetUserGroup().GetName() == "Admin" {
				res, _ := cli.serviceHot.FindHotel(ctx, &hotelpb.FindHotelRequest{
					Filter:               "",
					UserId:               0,
					XXX_NoUnkeyedLiteral: struct{}{},
					XXX_unrecognized:     nil,
					XXX_sizecache:        0,
				})

				for _, hot := range res.GetHotels() {
					hotels = append(hotels, &models.HotelShort{
						Id:   hot.GetId(),
						Name: hot.GetName(),
					})
				}
			}

			return hotels
		}(),
	}, nil
}

func (cli *UserClient) GetGroupsSelect(token string) (*[]models.Select, error){
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	resGrp, err := cli.serviceGrp.FindUserGroups(ctx, &userGrouppb.FindUserGroupsRequest{
		Filter:               "",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	groups := []models.Select{}

	for _, group := range resGrp.UserGroup {
		groups = append(groups, models.Select{
			Text:  group.Name,
			Value: group.Id,
		})
	}

	return &groups, err
}

func (cli *UserClient) CreateUser(userMap map[string]interface{}, token string) (models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	user := models.User{}
	data, err := json.Marshal(userMap)

	if err != nil {
		return user, err
	}
	if err := json.Unmarshal(data, &user); err != nil {
		return user, err
	}

	usr := &userpb.User{
		Id:                   user.Id,
		Username:             user.Username,
		Firstname:            user.Firstname,
		Lastname:             user.Lastname,
		Password:             user.Password,
		Email:                user.Email,
		Archived:             user.Archived,
		UserGroupId:          user.UserGroupId,
		Expiration:           nil,
		Description:          user.Description,
		Hotels:               func () []int32 {
			hotels := []int32{}
			if user.Hotels != nil {
				for _, hot := range user.Hotels {
					hotels = append(hotels, *hot)
				}
			}

			return hotels
		}(),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	if user.Id == 0 {
		res, err := cli.service.CreateUser(ctx, &userpb.CreateUserRequest{
			User:                 usr,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return user, err
		}

		user.Id = res.GetUser().GetId()
	}else {
		_, err := cli.service.UpdateUser(ctx, &userpb.UpdateUserRequest{
			User:                 usr,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return user, err
		}
	}

	return user, nil
}

func (u *UserClient) GetUser(id string, token string) (models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	idUser, err := strconv.Atoi(id)

	if err != nil {
		return models.User{}, err
	}

	res, err := u.service.GetUser(ctx, &userpb.GetUserRequest{
		Id:                   int32(idUser),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return models.User{}, err
	}

	user := res.GetUser()
	return models.User{
		Id:          user.GetId(),
		Username:    user.GetUsername(),
		Firstname:   user.GetFirstname(),
		Lastname:    user.GetLastname(),
		Password:    "",
		Email:       user.GetEmail(),
		Archived:    user.GetArchived(),
		UserGroupId: user.GetUserGroup().GetId(),
		Expiration:  user.GetExpiration(),
		Description: user.GetDescription(),
		Hotels:      func () []*int32 {
			if user.GetHotels() != nil {
				ids := []*int32{}
				for _, hot := range user.GetHotels() {
					ids = append(ids, &hot.Id)
				}
				return ids
			}
			return nil
		}(),
		Group:       nil,
	}, nil
}

func (u *UserClient) DeleteUser(id int32, token string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	//idUser, err := strconv.Atoi(id)

	log.Println("El id de usuario es: %v", id)
	//if err != nil {
	//	return err
	//}

	req := &userpb.DeleteUserRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	_, err := u.service.DeleteUser(ctx, req)

	if err != nil {
		return err
	}

	return nil
}

func (u *UserClient) FindUsers(filter string, token string) (*[]models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	req := &userpb.FindUsersRequest{
		Filter:               strings.TrimSpace(filter),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	res, err := u.service.FindUsers(ctx, req)

	if err != nil {
		return nil, err
	}

	users := []models.User{}

	for _, usr := range res.GetUser() {
		users = append(users, models.User{
			Id:          usr.GetId(),
			Username:    usr.GetUsername(),
			Firstname:   usr.GetFirstname(),
			Lastname:    usr.GetLastname(),
			Password:    usr.GetPassword(),
			Email:       usr.GetEmail(),
			Archived:    usr.GetArchived(),
			UserGroupId: 0,
			Expiration:  usr.GetExpiration(),
			Description: usr.GetDescription(),
			Hotels:      nil,
			Group: 		 func () *string {
				if usr.GetUserGroup() != nil {
					group := usr.GetUserGroup().GetName()
					return &group
				}

				return nil
			}(),
		})
	}

	return &users, nil
}
