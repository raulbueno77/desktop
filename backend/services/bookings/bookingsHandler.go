package bookings

import (
	"cehotel/backend/models"
	bookingpb "cehotel/backend/proto/booking"
	clientpb "cehotel/backend/proto/client"
	roompb "cehotel/backend/proto/room"
	"cehotel/backend/utils"
	"context"
	"encoding/json"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"log"
	"strings"
	"time"
)

type BookingClient struct {
	service 		bookingpb.BookingServicesClient
	serviceClient	clientpb.ClientServicesClient
	serviceRoom		roompb.RoomServicesClient
}

func NewBookingClient (cc *grpc.ClientConn) *BookingClient {
	service := bookingpb.NewBookingServicesClient(cc)
	serviceClient := clientpb.NewClientServicesClient(cc)
	serviceRoom := roompb.NewRoomServicesClient(cc)

	return &BookingClient{
		service: service,
		serviceClient: serviceClient,
		serviceRoom: serviceRoom,
	}
}

func (c *BookingClient) CheckDates (checkIn string, checkOut string, roomName string, id int32, hotelId int32, token string) (bool, error) {
	log.Println("Check dates Type...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	res, err := c.service.CheckDates(ctx, &bookingpb.CheckDatesRequest{
		DateBegin:            checkIn,
		DateEnd:              checkOut,
		BookingId:            id,
		RoomId:               0,
		RoomName:             roomName,
		HotelId: 			  hotelId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return false, err
	}

	return res.Status, nil
}

func (c *BookingClient) CreateBooking (bookingNewMap map[string]interface{}, hotelId int32, token string) (*models.NewBooking, error) {
	log.Println("Create Booking...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	bookingType := models.NewBooking{}
	data, err := json.Marshal(bookingNewMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data, &bookingType); err != nil {
		return nil, err
	}

	//check if the dates are free
	res, err := c.service.CheckDates(ctx, &bookingpb.CheckDatesRequest{
		DateBegin:            bookingType.CheckIn,
		DateEnd:              bookingType.CheckOut,
		BookingId:            bookingType.Id,
		RoomId:				  0,
		RoomName: 			  bookingType.RoomName,
		HotelId: 			  hotelId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	if !res.GetStatus() {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprint("Error dates"))
	}

	//search the client first
	resCli, err := c.serviceClient.GetClient(ctx, &clientpb.GetClientRequest{
		Id:                   bookingType.Client.Id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		//create client
		document := ""
		if bookingType.Client.Document != "" {
			document = utils.FormatDocument(bookingType.Client.Document)
		}
		res, err := c.serviceClient.CreateClient(ctx, &clientpb.CreateClientRequest{
			Client:               &clientpb.Client{
				FirstName:            bookingType.Client.FirstName,
				LastName:             bookingType.Client.LastName,
				SecondLastName:       bookingType.Client.SecondLastName,
				BirthDate:            bookingType.Client.BirthDate,
				Document:             document,
				DocumentType:         bookingType.Client.DocumentType,
				Gender:               bookingType.Client.Gender,
				Address:              bookingType.Client.Address,
				PostalCode:           bookingType.Client.PostalCode,
				Country:              bookingType.Client.Country,
				Region:               bookingType.Client.Region,
				City:                 bookingType.Client.City,
				HomePhone:            bookingType.Client.HomePhone,
				CellPhone:            bookingType.Client.CellPhone,
				Email:                bookingType.Client.Email,
				CreditCardNumber:     bookingType.Client.CreditCardNumber,
				CreditCardMonth:      bookingType.Client.CreditCardMonth,
				CreditCardYear:       bookingType.Client.CreditCardYear,
				CreditCardSecurity:   bookingType.Client.CreditCardSecurity,
				DocumentDate:         bookingType.Client.DocumentDate,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return nil, err
		}

		bookingType.Client.Id = res.GetClient().GetId()
	}else {
		log.Println("Updating client...")
		log.Println(bookingType.Client)
		_, err := c.serviceClient.UpdateClient(ctx, &clientpb.UpdateClientRequest{
			Client:               &clientpb.Client{
				Id:					  resCli.GetClient().GetId(),
				FirstName:            bookingType.Client.FirstName,
				LastName:             bookingType.Client.LastName,
				SecondLastName:       bookingType.Client.SecondLastName,
				BirthDate:            bookingType.Client.BirthDate,
				Document:             bookingType.Client.Document,
				DocumentType:         bookingType.Client.DocumentType,
				Gender:               bookingType.Client.Gender,
				Address:              bookingType.Client.Address,
				PostalCode:           bookingType.Client.PostalCode,
				Country:              bookingType.Client.Country,
				Region:               bookingType.Client.Region,
				City:                 bookingType.Client.City,
				HomePhone:            bookingType.Client.HomePhone,
				CellPhone:            bookingType.Client.CellPhone,
				Email:                bookingType.Client.Email,
				CreditCardNumber:     bookingType.Client.CreditCardNumber,
				CreditCardMonth:      bookingType.Client.CreditCardMonth,
				CreditCardYear:       bookingType.Client.CreditCardYear,
				CreditCardSecurity:   bookingType.Client.CreditCardSecurity,
				DocumentDate:         bookingType.Client.DocumentDate,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return nil, err
		}
	}

	//search room
	resRoom, err :=c.serviceRoom.GetRoom(ctx, &roompb.GetRoomRequest{
		Id:                   0,
		Name:                 bookingType.RoomName,
		HotelId: 			  hotelId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	bookingId := int32(0)

	if bookingType.Id == 0 {
		resBook, err := c.service.CreateBooking(ctx, &bookingpb.CreateBookingRequest{
			Booking:              &bookingpb.Booking{
				ClientId:             bookingType.Client.Id,
				RoomId:               resRoom.GetRoom().GetId(),
				Amount:               bookingType.Price,
				DateBegin:            bookingType.CheckIn,
				DateEnd:              bookingType.CheckOut,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return nil, err
		}

		bookingId = resBook.GetBooking().GetId()
	}else {
		resBook, err := c.service.UpdateBooking(ctx, &bookingpb.UpdateBookingRequest{
			Booking:              &bookingpb.Booking{
				Id: 				  bookingType.Id,
				ClientId:             bookingType.Client.Id,
				RoomId:               resRoom.GetRoom().GetId(),
				Amount:               bookingType.Price,
				DateBegin:            bookingType.CheckIn,
				DateEnd:              bookingType.CheckOut,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return nil, err
		}

		bookingId = resBook.GetBooking().GetId()
	}

	if err != nil {
		return nil, err
	}


	//save client as guest if the requiremnts are all right
	resGuests, err := c.service.FindGuests(ctx, &bookingpb.FindGuestsRequest{
		BookingId:            bookingId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	exists := false

	for _, row := range resGuests.GetGuests() {
		if row.GetDocument() == bookingType.Client.Document {
			exists = true
			break
		}
	}

	if !exists {
		if bookingType.Client.Document != "" && bookingType.Client.DocumentDate != "" && bookingType.Client.DocumentType != "" &&
			bookingType.Client.FirstName != "" && bookingType.Client.LastName != "" && bookingType.Client.BirthDate != "" &&
			bookingType.Client.Gender != "" && bookingType.Client.Country != "" {
			//create guest
			_, err := c.service.CreateGuest(ctx, &bookingpb.CreateGuestRequest{
				Guest:                &bookingpb.Guest{
					FirstName:            bookingType.Client.FirstName,
					LastName:             bookingType.Client.LastName,
					SecondLastName:       bookingType.Client.SecondLastName,
					BirthDate:            bookingType.Client.BirthDate,
					Document:             bookingType.Client.Document,
					DocumentType:         bookingType.Client.DocumentType,
					Gender:               bookingType.Client.Gender,
					Country:              bookingType.Client.Country,
					DocumentDate:         bookingType.Client.DocumentDate,
					BookingId:            bookingId,
					CheckIn:              bookingType.CheckIn,
					XXX_NoUnkeyedLiteral: struct{}{},
					XXX_unrecognized:     nil,
					XXX_sizecache:        0,
				},
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			})

			if err != nil {
				log.Println(err)
			}
		}
	}

	return &bookingType, nil
}

func (c *BookingClient) GetBooking (id int32, roomName string, begin string, hotelId int32, token string) (*models.NewBooking, error) {
	log.Println("Create Booking...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := c.service.GetBooking(ctx, &bookingpb.GetBookingRequest{
		Id:                   id,
		RoomName: 			  roomName,
		Begin: 				  begin,
		HotelId: 			  hotelId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	booking := &models.NewBooking{
		Id:		  res.GetBooking().GetId(),
		Number:   func () string {
			aux := strings.Split(res.GetBooking().GetDateBegin(), "-")

			return fmt.Sprintf("N%v/%v", res.GetBooking().GetNumber(), aux[0])
		}(),
		RoomName: res.GetBooking().GetRoom().GetName(),
		CheckIn:  res.GetBooking().GetDateBegin(),
		CheckOut: res.GetBooking().GetDateEnd(),
		CheckInDate: res.GetBooking().GetCheckin(),
		Price:    res.GetBooking().GetAmount(),
		Client:   models.Client{
			Id:                 res.GetBooking().GetClient().GetId(),
			FirstName:          res.GetBooking().GetClient().GetFirstName(),
			LastName:           res.GetBooking().GetClient().GetLastName(),
			SecondLastName:     res.GetBooking().GetClient().GetSecondLastName(),
			BirthDate:          res.GetBooking().GetClient().GetBirthDate(),
			Document:           res.GetBooking().GetClient().GetDocument(),
			DocumentType:       res.GetBooking().GetClient().GetDocumentType(),
			Gender:             res.GetBooking().GetClient().GetGender(),
			Address:            res.GetBooking().GetClient().GetAddress(),
			PostalCode:         res.GetBooking().GetClient().GetPostalCode(),
			Country:            res.GetBooking().GetClient().GetCountry(),
			Region:             res.GetBooking().GetClient().GetRegion(),
			City:               res.GetBooking().GetClient().GetCity(),
			HomePhone:          res.GetBooking().GetClient().GetHomePhone(),
			CellPhone:          res.GetBooking().GetClient().GetCellPhone(),
			Email:              res.GetBooking().GetClient().GetEmail(),
			CreditCardNumber:   res.GetBooking().GetClient().GetCreditCardNumber(),
			CreditCardMonth:    res.GetBooking().GetClient().GetCreditCardMonth(),
			CreditCardYear:     res.GetBooking().GetClient().GetCreditCardYear(),
			CreditCardSecurity: res.GetBooking().GetClient().GetCreditCardSecurity(),
			Observations:       res.GetBooking().GetClient().GetObservations(),
			DocumentDate:       res.GetBooking().GetClient().GetDocumentDate(),
		},
	}

	return booking, nil
}

func (c *BookingClient) CreateGuest (guestMap map[string]interface{}, token string) (*models.Guest, error) {
	log.Println("Create Guest...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	guestType := models.Guest{}
	data, err := json.Marshal(guestMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data, &guestType); err != nil {
		return nil, err
	}

	_, err = c.service.CreateGuest(ctx, &bookingpb.CreateGuestRequest{
		Guest:                &bookingpb.Guest{
			FirstName:            guestType.FirstName,
			LastName:             guestType.LastName,
			SecondLastName:       guestType.SecondLastName,
			BirthDate:            guestType.BirthDate,
			Document:             utils.FormatDocument(guestType.Document),
			DocumentType:         guestType.DocumentType,
			Gender:               guestType.Gender,
			Country:              guestType.Country,
			DocumentDate:         guestType.DocumentDate,
			BookingId:            guestType.BookingId,
			CheckIn: 			  guestType.CheckIn,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	return &guestType, nil
}

func (c *BookingClient) UpdateGuest (guestMap map[string]interface{}, token string) (*models.Guest, error) {
	log.Println("Update Guest...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	guestType := models.Guest{}
	data, err := json.Marshal(guestMap)

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(data, &guestType); err != nil {
		return nil, err
	}

	_, err = c.service.UpdateGuest(ctx, &bookingpb.UpdateGuestRequest{
		Guest:                &bookingpb.Guest{
			Id:					  guestType.Id,
			FirstName:            guestType.FirstName,
			LastName:             guestType.LastName,
			SecondLastName:       guestType.SecondLastName,
			BirthDate:            guestType.BirthDate,
			Document:             utils.FormatDocument(guestType.Document),
			DocumentType:         guestType.DocumentType,
			Gender:               guestType.Gender,
			Country:              guestType.Country,
			DocumentDate:         guestType.DocumentDate,
			BookingId:            guestType.BookingId,
			CheckIn: 			  guestType.CheckIn,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	return &guestType, nil
}

func (c *BookingClient) FindGuests (bookingId int32, token string) ([]models.Guest, error) {
	log.Println("Find Booking Guests...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := c.service.FindGuests(ctx, &bookingpb.FindGuestsRequest{
		BookingId:            bookingId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	guests := []models.Guest{}

	for _, row := range res.GetGuests() {
		guests = append(guests, models.Guest{
			Id:             row.GetId(),
			BookingId:      row.GetBookingId(),
			FirstName:      row.GetFirstName(),
			LastName:       row.GetLastName(),
			SecondLastName: row.GetSecondLastName(),
			BirthDate:      row.GetBirthDate(),
			Document:       row.GetDocument(),
			DocumentType:   row.GetDocumentType(),
			Gender:         row.GetGender(),
			Country:        row.GetCountry(),
			DocumentDate:   row.DocumentDate,
			CheckIn:        row.CheckIn,
		})
	}

	return guests, nil
}

func (c *BookingClient) GetGuest (guestId int32, token string) (*models.Guest, error) {
	log.Println("Get Guest...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := c.service.GetGuest(ctx, &bookingpb.GetGuestRequest{
		Id:            guestId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}
	
	return &models.Guest{
		Id:             res.GetGuest().GetId(),
		BookingId:      res.GetGuest().GetBookingId(),
		FirstName:      res.GetGuest().GetFirstName(),
		LastName:       res.GetGuest().GetLastName(),
		SecondLastName: res.GetGuest().GetSecondLastName(),
		BirthDate:      res.GetGuest().GetBirthDate(),
		Document:       res.GetGuest().GetDocument(),
		DocumentType:   res.GetGuest().GetDocumentType(),
		Gender:         res.GetGuest().GetGender(),
		Country:        res.GetGuest().GetCountry(),
		DocumentDate:   res.GetGuest().DocumentDate,
		CheckIn:        res.GetGuest().CheckIn,
	}, nil
}

func (c *BookingClient) DeleteGuest (guestId int32, token string) (*string, error) {
	log.Println("Delete Guest...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	_, err := c.service.DeleteGuest(ctx, &bookingpb.DeleteGuestRequest{
		Id:            guestId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	toReturn :=  "operation ok"

	return &toReturn, nil
}

func (c *BookingClient) AddCheckin (bookingId int32, token string) (*string, error) {
	log.Println("Add checking...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	res, err := c.service.ManageCheckin(ctx, &bookingpb.ManageCheckinRequest{
		Checkin:              &bookingpb.Checkin{
			BookingId:            bookingId,
			In:                   true,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	toReturn := res.GetCheckin().GetDate()

	return &toReturn, nil
}

func (c *BookingClient) RemoveCheckin (bookingId int32, token string) (*string, error) {
	log.Println("Add checking...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	_, err := c.service.ManageCheckin(ctx, &bookingpb.ManageCheckinRequest{
		Checkin:              &bookingpb.Checkin{
			BookingId:            bookingId,
			In:                   false,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	toReturn := "operation ok"

	return &toReturn, nil
}

func (c *BookingClient) DeleteBooking(bookingId int32, token string) (*string, error) {
	log.Println("Add checking...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	_, err := c.service.DeleteBooking(ctx, &bookingpb.DeleteBookingRequest{
		Id:                   bookingId,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return nil, err
	}

	toReturn := "operation ok"

	return &toReturn, nil
}
