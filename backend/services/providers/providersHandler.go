package providers

import (
	"cehotel/backend/models"
	providerpb "cehotel/backend/proto/provider"
	"encoding/json"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"log"
	"strconv"
	"strings"
	"time"
	"context"
)

type ProvidersClient struct {
	service providerpb.ProviderServicesClient
}

func NewProvidersClient (cc *grpc.ClientConn) *ProvidersClient {
	service := providerpb.NewProviderServicesClient(cc)

	return &ProvidersClient{service: service}
}

func (c *ProvidersClient) CreateProvider(providerMap map[string]interface{}, token string) (models.Provider, error) {
	log.Println("Create or Update Provider...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	provider := models.Provider{}
	data, err := json.Marshal(providerMap)

	if err != nil {
		return provider, err
	}

	if err := json.Unmarshal(data, &provider); err != nil {
		return provider, err
	}

	providerGrpc := provider.Parse()

	if providerGrpc.Id == 0 {
		res, err := c.service.CreateProvider(ctx, &providerpb.CreateProviderRequest {
			Provider:             providerGrpc,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return provider, err
		}

		provider.Id = res.GetProvider().GetId()
	}else {
		_, err := c.service.UpdateProvider(ctx, &providerpb.UpdateProviderRequest{
			Provider:             providerGrpc,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return provider, err
		}
	}

	return provider, nil
}

func (c *ProvidersClient) GetProvider(id string, token string) (models.Provider, error) {
	log.Println("Get Provider...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	provider := models.Provider{}

	idProvider, err := strconv.Atoi(id)

	if err != nil {
		return models.Provider{}, err
	}


	res, err := c.service.GetProvider(ctx, &providerpb.GetProviderRequest{
		Id:                   int32(idProvider),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return provider, err
	}

	provider.Hydrate(res.GetProvider())

	return provider, nil
}

func (c *ProvidersClient) FindProviders(ftr string, page int32, sort string, sortDirection string,token string) (models.ProviderList, error) {
	log.Println("Find Providers...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	providers := []models.ListProviderView{}

	res, err := c.service.FindProviders(ctx, &providerpb.FindProvidersRequest{
		Filter:               ftr,
		Page:                 page,
		Sort:                 sort,
		SortDirection:        sortDirection,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return models.ProviderList{}, err
	}

	for _, prov := range res.GetProviders() {
		providers = append(providers, models.ListProviderView{
			Id:       prov.GetId(),
			Name:     strings.TrimSpace(prov.GetName()),
			Document: prov.GetDocument(),
			Place:    strings.TrimSpace(prov.GetCountry() + " " + prov.GetRegion()),
			Phones:   func () string {
				phone := ""

				if prov.GetJobPhone() != "" && prov.GetCellPhone() != "" {
					phone = prov.GetJobPhone() + " / " + prov.GetCellPhone()
				}else {
					if prov.GetJobPhone() != "" {
						phone = prov.GetJobPhone()
					}else {
						phone = prov.GetCellPhone()
					}
				}

				return phone
			}(),
			Email:    prov.GetEmail(),
		})
	}

	pages := []models.Select{}

	for i := 1; i <= int(res.GetTotalPages()); i++ {
		pages = append(pages, models.Select{
			Text:  strconv.Itoa(i) + " de " + strconv.Itoa(int(res.GetTotalPages())),
			Value: int32(i),
		})
	}

	return models.ProviderList{
		Provider:   providers,
		TotalPages: res.GetTotalPages(),
		SelectPages: pages,
	}, nil
}

func (c *ProvidersClient) DeleteProvider (id int32, token string) error {
	log.Println("Delete Client...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	_, err := c.service.DeleteProvider(ctx, &providerpb.DeleteProviderRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return err
	}

	return nil
}