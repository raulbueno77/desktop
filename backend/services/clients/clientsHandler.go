package clients

import (
	"cehotel/backend/models"
	clientpb "cehotel/backend/proto/client"
	"cehotel/backend/utils"
	"context"
	"encoding/json"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"log"
	"strconv"
	"strings"
	"time"
)

type ClientsClient struct {
	service clientpb.ClientServicesClient
}

func NewClientsClient (cc *grpc.ClientConn) *ClientsClient {
	service := clientpb.NewClientServicesClient(cc)

	return &ClientsClient{service: service}
}

func (c *ClientsClient) CreateClient(clientMap map[string]interface{}, token string) (models.Client, error) {
	log.Println("Create or Update Client...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	client := models.Client{}
	data, err := json.Marshal(clientMap)

	if err != nil {
		return client, err
	}

	if err := json.Unmarshal(data, &client); err != nil {
		return client, err
	}

	clientGrpc := client.Parse()

	if clientGrpc.Id == 0 {
		res, err := c.service.CreateClient(ctx, &clientpb.CreateClientRequest{
			Client:               clientGrpc,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return client, err
		}

		client.Id = res.GetClient().GetId()
	}else {
		_, err := c.service.UpdateClient(ctx, &clientpb.UpdateClientRequest{
			Client:               clientGrpc,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})

		if err != nil {
			return client, err
		}
	}

	client.Document = clientGrpc.GetDocument()

	return client, nil
}

func (c *ClientsClient) GetClient(id string, token string) (models.Client, error) {
	log.Println("Get Client...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	client := models.Client{}

	idClient, err := strconv.Atoi(id)

	if err != nil {
		return models.Client{}, err
	}


	res, err := c.service.GetClient(ctx, &clientpb.GetClientRequest{
		Id:                   int32(idClient),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return client, err
	}

	client.Hydrate(res.GetClient())

	return client, nil
}

func (c *ClientsClient) FindClients(ftr string, page int32, sort string, sortDirection string,token string) (models.ClientList, error) {
	log.Println("Find Clients...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	clients := []models.ListClientView{}

	res, err := c.service.FindClients(ctx, &clientpb.FindClientsRequest{
		Filter:               ftr,
		Page:                 page,
		Sort:                 sort,
		SortDirection:        sortDirection,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return models.ClientList{}, err
	}

	for _, cli := range res.GetClients() {
		clients = append(clients, models.ListClientView{
			Id:       cli.GetId(),
			Name:     strings.TrimSpace(cli.GetFirstName() + " " + cli.GetLastName() + " " + cli.GetSecondLastName()),
			Document: cli.GetDocument(),
			Place:    strings.TrimSpace(cli.GetCountry() + " " + cli.GetRegion()),
			Phones:   func () string {
				phone := ""

				if cli.GetHomePhone() != "" && cli.GetCellPhone() != "" {
					phone = cli.GetHomePhone() + " / " + cli.GetCellPhone()
				}else {
					if cli.GetHomePhone() != "" {
						phone = cli.GetHomePhone()
					}else {
						phone = cli.GetCellPhone()
					}
				}

				return phone
			}(),
			Email:    cli.GetEmail(),
		})
	}

	pages := []models.Select{}

	for i := 1; i <= int(res.GetTotalPages()); i++ {
		pages = append(pages, models.Select{
			Text:  strconv.Itoa(i) + " de " + strconv.Itoa(int(res.GetTotalPages())),
			Value: int32(i),
		})
	}

	return models.ClientList{
		Clients:   clients,
		TotalPages: res.GetTotalPages(),
		SelectPages: pages,
	}, nil
}

func (c *ClientsClient) DeleteClient (id int32, token string) error {
	log.Println("Delete Client...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)

	_, err := c.service.DeleteClient(ctx, &clientpb.DeleteClientRequest{
		Id:                   id,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})

	if err != nil {
		return err
	}

	return nil
}

func (c *ClientsClient) GetClientByDocument (document string, token string) (models.Client, error) {
	log.Println("Get Client...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", token)
	client := models.Client{}
	
	res, err := c.service.GetClientByDocument(ctx, &clientpb.GetClientByDocumentRequest{
		Document:             utils.FormatDocument(document),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	})
	if err != nil {
		return client, err
	}

	client.Hydrate(res.GetClient())

	return client, nil
}