package utils

import (
	"os/exec"
	"strings"
)

const alpha = "abcdefghijklmnñopqrstuvwxyz0123456789"

func FormatDocument(s string) string {

	toReturn := ""

	for _, char := range s {
		if strings.Contains(alpha, strings.ToLower(string(char))) {
			toReturn += strings.ToUpper(string(char))
		}
	}

	return toReturn
}

func ExecuteShell (app string, args ...string) (*string, error) {
	cmd := exec.Command(app, args...)
	stdout, err := cmd.Output()

	if err != nil {
		return nil, err
	}

	toReturn := string(stdout)

	return &toReturn, nil
}
