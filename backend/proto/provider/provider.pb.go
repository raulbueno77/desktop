// Code generated by protoc-gen-go. DO NOT EDIT.
// source: backend/proto/provider/provider.proto

package providerpb

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Provider struct {
	Id                   int32    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Document             string   `protobuf:"bytes,3,opt,name=document,proto3" json:"document,omitempty"`
	Address              string   `protobuf:"bytes,4,opt,name=address,proto3" json:"address,omitempty"`
	PostalCode           string   `protobuf:"bytes,5,opt,name=postal_code,json=postalCode,proto3" json:"postal_code,omitempty"`
	Country              string   `protobuf:"bytes,6,opt,name=country,proto3" json:"country,omitempty"`
	Region               string   `protobuf:"bytes,7,opt,name=region,proto3" json:"region,omitempty"`
	City                 string   `protobuf:"bytes,8,opt,name=city,proto3" json:"city,omitempty"`
	JobPhone             string   `protobuf:"bytes,9,opt,name=job_phone,json=jobPhone,proto3" json:"job_phone,omitempty"`
	CellPhone            string   `protobuf:"bytes,10,opt,name=cell_phone,json=cellPhone,proto3" json:"cell_phone,omitempty"`
	Email                string   `protobuf:"bytes,11,opt,name=email,proto3" json:"email,omitempty"`
	Observations         string   `protobuf:"bytes,12,opt,name=observations,proto3" json:"observations,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Provider) Reset()         { *m = Provider{} }
func (m *Provider) String() string { return proto.CompactTextString(m) }
func (*Provider) ProtoMessage()    {}
func (*Provider) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{0}
}

func (m *Provider) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Provider.Unmarshal(m, b)
}
func (m *Provider) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Provider.Marshal(b, m, deterministic)
}
func (m *Provider) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Provider.Merge(m, src)
}
func (m *Provider) XXX_Size() int {
	return xxx_messageInfo_Provider.Size(m)
}
func (m *Provider) XXX_DiscardUnknown() {
	xxx_messageInfo_Provider.DiscardUnknown(m)
}

var xxx_messageInfo_Provider proto.InternalMessageInfo

func (m *Provider) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Provider) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Provider) GetDocument() string {
	if m != nil {
		return m.Document
	}
	return ""
}

func (m *Provider) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

func (m *Provider) GetPostalCode() string {
	if m != nil {
		return m.PostalCode
	}
	return ""
}

func (m *Provider) GetCountry() string {
	if m != nil {
		return m.Country
	}
	return ""
}

func (m *Provider) GetRegion() string {
	if m != nil {
		return m.Region
	}
	return ""
}

func (m *Provider) GetCity() string {
	if m != nil {
		return m.City
	}
	return ""
}

func (m *Provider) GetJobPhone() string {
	if m != nil {
		return m.JobPhone
	}
	return ""
}

func (m *Provider) GetCellPhone() string {
	if m != nil {
		return m.CellPhone
	}
	return ""
}

func (m *Provider) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *Provider) GetObservations() string {
	if m != nil {
		return m.Observations
	}
	return ""
}

type ProviderView struct {
	Id                   int32    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Document             string   `protobuf:"bytes,3,opt,name=document,proto3" json:"document,omitempty"`
	Address              string   `protobuf:"bytes,4,opt,name=address,proto3" json:"address,omitempty"`
	PostalCode           string   `protobuf:"bytes,5,opt,name=postal_code,json=postalCode,proto3" json:"postal_code,omitempty"`
	Country              string   `protobuf:"bytes,6,opt,name=country,proto3" json:"country,omitempty"`
	Region               string   `protobuf:"bytes,7,opt,name=region,proto3" json:"region,omitempty"`
	City                 string   `protobuf:"bytes,8,opt,name=city,proto3" json:"city,omitempty"`
	JobPhone             string   `protobuf:"bytes,9,opt,name=job_phone,json=jobPhone,proto3" json:"job_phone,omitempty"`
	CellPhone            string   `protobuf:"bytes,10,opt,name=cell_phone,json=cellPhone,proto3" json:"cell_phone,omitempty"`
	Email                string   `protobuf:"bytes,11,opt,name=email,proto3" json:"email,omitempty"`
	Observations         string   `protobuf:"bytes,12,opt,name=observations,proto3" json:"observations,omitempty"`
	Created              string   `protobuf:"bytes,13,opt,name=created,proto3" json:"created,omitempty"`
	Modified             string   `protobuf:"bytes,14,opt,name=modified,proto3" json:"modified,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ProviderView) Reset()         { *m = ProviderView{} }
func (m *ProviderView) String() string { return proto.CompactTextString(m) }
func (*ProviderView) ProtoMessage()    {}
func (*ProviderView) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{1}
}

func (m *ProviderView) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ProviderView.Unmarshal(m, b)
}
func (m *ProviderView) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ProviderView.Marshal(b, m, deterministic)
}
func (m *ProviderView) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ProviderView.Merge(m, src)
}
func (m *ProviderView) XXX_Size() int {
	return xxx_messageInfo_ProviderView.Size(m)
}
func (m *ProviderView) XXX_DiscardUnknown() {
	xxx_messageInfo_ProviderView.DiscardUnknown(m)
}

var xxx_messageInfo_ProviderView proto.InternalMessageInfo

func (m *ProviderView) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *ProviderView) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *ProviderView) GetDocument() string {
	if m != nil {
		return m.Document
	}
	return ""
}

func (m *ProviderView) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

func (m *ProviderView) GetPostalCode() string {
	if m != nil {
		return m.PostalCode
	}
	return ""
}

func (m *ProviderView) GetCountry() string {
	if m != nil {
		return m.Country
	}
	return ""
}

func (m *ProviderView) GetRegion() string {
	if m != nil {
		return m.Region
	}
	return ""
}

func (m *ProviderView) GetCity() string {
	if m != nil {
		return m.City
	}
	return ""
}

func (m *ProviderView) GetJobPhone() string {
	if m != nil {
		return m.JobPhone
	}
	return ""
}

func (m *ProviderView) GetCellPhone() string {
	if m != nil {
		return m.CellPhone
	}
	return ""
}

func (m *ProviderView) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *ProviderView) GetObservations() string {
	if m != nil {
		return m.Observations
	}
	return ""
}

func (m *ProviderView) GetCreated() string {
	if m != nil {
		return m.Created
	}
	return ""
}

func (m *ProviderView) GetModified() string {
	if m != nil {
		return m.Modified
	}
	return ""
}

type CreateProviderRequest struct {
	Provider             *Provider `protobuf:"bytes,1,opt,name=provider,proto3" json:"provider,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *CreateProviderRequest) Reset()         { *m = CreateProviderRequest{} }
func (m *CreateProviderRequest) String() string { return proto.CompactTextString(m) }
func (*CreateProviderRequest) ProtoMessage()    {}
func (*CreateProviderRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{2}
}

func (m *CreateProviderRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateProviderRequest.Unmarshal(m, b)
}
func (m *CreateProviderRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateProviderRequest.Marshal(b, m, deterministic)
}
func (m *CreateProviderRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateProviderRequest.Merge(m, src)
}
func (m *CreateProviderRequest) XXX_Size() int {
	return xxx_messageInfo_CreateProviderRequest.Size(m)
}
func (m *CreateProviderRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateProviderRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateProviderRequest proto.InternalMessageInfo

func (m *CreateProviderRequest) GetProvider() *Provider {
	if m != nil {
		return m.Provider
	}
	return nil
}

type CreateProviderResponse struct {
	Provider             *ProviderView `protobuf:"bytes,1,opt,name=provider,proto3" json:"provider,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *CreateProviderResponse) Reset()         { *m = CreateProviderResponse{} }
func (m *CreateProviderResponse) String() string { return proto.CompactTextString(m) }
func (*CreateProviderResponse) ProtoMessage()    {}
func (*CreateProviderResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{3}
}

func (m *CreateProviderResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateProviderResponse.Unmarshal(m, b)
}
func (m *CreateProviderResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateProviderResponse.Marshal(b, m, deterministic)
}
func (m *CreateProviderResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateProviderResponse.Merge(m, src)
}
func (m *CreateProviderResponse) XXX_Size() int {
	return xxx_messageInfo_CreateProviderResponse.Size(m)
}
func (m *CreateProviderResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateProviderResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CreateProviderResponse proto.InternalMessageInfo

func (m *CreateProviderResponse) GetProvider() *ProviderView {
	if m != nil {
		return m.Provider
	}
	return nil
}

type UpdateProviderRequest struct {
	Provider             *Provider `protobuf:"bytes,1,opt,name=provider,proto3" json:"provider,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *UpdateProviderRequest) Reset()         { *m = UpdateProviderRequest{} }
func (m *UpdateProviderRequest) String() string { return proto.CompactTextString(m) }
func (*UpdateProviderRequest) ProtoMessage()    {}
func (*UpdateProviderRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{4}
}

func (m *UpdateProviderRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateProviderRequest.Unmarshal(m, b)
}
func (m *UpdateProviderRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateProviderRequest.Marshal(b, m, deterministic)
}
func (m *UpdateProviderRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateProviderRequest.Merge(m, src)
}
func (m *UpdateProviderRequest) XXX_Size() int {
	return xxx_messageInfo_UpdateProviderRequest.Size(m)
}
func (m *UpdateProviderRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateProviderRequest.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateProviderRequest proto.InternalMessageInfo

func (m *UpdateProviderRequest) GetProvider() *Provider {
	if m != nil {
		return m.Provider
	}
	return nil
}

type UpdateProviderResponse struct {
	Provider             *ProviderView `protobuf:"bytes,1,opt,name=provider,proto3" json:"provider,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *UpdateProviderResponse) Reset()         { *m = UpdateProviderResponse{} }
func (m *UpdateProviderResponse) String() string { return proto.CompactTextString(m) }
func (*UpdateProviderResponse) ProtoMessage()    {}
func (*UpdateProviderResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{5}
}

func (m *UpdateProviderResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateProviderResponse.Unmarshal(m, b)
}
func (m *UpdateProviderResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateProviderResponse.Marshal(b, m, deterministic)
}
func (m *UpdateProviderResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateProviderResponse.Merge(m, src)
}
func (m *UpdateProviderResponse) XXX_Size() int {
	return xxx_messageInfo_UpdateProviderResponse.Size(m)
}
func (m *UpdateProviderResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateProviderResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateProviderResponse proto.InternalMessageInfo

func (m *UpdateProviderResponse) GetProvider() *ProviderView {
	if m != nil {
		return m.Provider
	}
	return nil
}

type DeleteProviderRequest struct {
	Id                   int32    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteProviderRequest) Reset()         { *m = DeleteProviderRequest{} }
func (m *DeleteProviderRequest) String() string { return proto.CompactTextString(m) }
func (*DeleteProviderRequest) ProtoMessage()    {}
func (*DeleteProviderRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{6}
}

func (m *DeleteProviderRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteProviderRequest.Unmarshal(m, b)
}
func (m *DeleteProviderRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteProviderRequest.Marshal(b, m, deterministic)
}
func (m *DeleteProviderRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteProviderRequest.Merge(m, src)
}
func (m *DeleteProviderRequest) XXX_Size() int {
	return xxx_messageInfo_DeleteProviderRequest.Size(m)
}
func (m *DeleteProviderRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteProviderRequest.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteProviderRequest proto.InternalMessageInfo

func (m *DeleteProviderRequest) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

type DeleteProviderResponse struct {
	Message              string   `protobuf:"bytes,1,opt,name=message,proto3" json:"message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteProviderResponse) Reset()         { *m = DeleteProviderResponse{} }
func (m *DeleteProviderResponse) String() string { return proto.CompactTextString(m) }
func (*DeleteProviderResponse) ProtoMessage()    {}
func (*DeleteProviderResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{7}
}

func (m *DeleteProviderResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteProviderResponse.Unmarshal(m, b)
}
func (m *DeleteProviderResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteProviderResponse.Marshal(b, m, deterministic)
}
func (m *DeleteProviderResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteProviderResponse.Merge(m, src)
}
func (m *DeleteProviderResponse) XXX_Size() int {
	return xxx_messageInfo_DeleteProviderResponse.Size(m)
}
func (m *DeleteProviderResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteProviderResponse.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteProviderResponse proto.InternalMessageInfo

func (m *DeleteProviderResponse) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

type GetProviderRequest struct {
	Id                   int32    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetProviderRequest) Reset()         { *m = GetProviderRequest{} }
func (m *GetProviderRequest) String() string { return proto.CompactTextString(m) }
func (*GetProviderRequest) ProtoMessage()    {}
func (*GetProviderRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{8}
}

func (m *GetProviderRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetProviderRequest.Unmarshal(m, b)
}
func (m *GetProviderRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetProviderRequest.Marshal(b, m, deterministic)
}
func (m *GetProviderRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetProviderRequest.Merge(m, src)
}
func (m *GetProviderRequest) XXX_Size() int {
	return xxx_messageInfo_GetProviderRequest.Size(m)
}
func (m *GetProviderRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetProviderRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetProviderRequest proto.InternalMessageInfo

func (m *GetProviderRequest) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

type GetProviderResponse struct {
	Provider             *ProviderView `protobuf:"bytes,1,opt,name=provider,proto3" json:"provider,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *GetProviderResponse) Reset()         { *m = GetProviderResponse{} }
func (m *GetProviderResponse) String() string { return proto.CompactTextString(m) }
func (*GetProviderResponse) ProtoMessage()    {}
func (*GetProviderResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{9}
}

func (m *GetProviderResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetProviderResponse.Unmarshal(m, b)
}
func (m *GetProviderResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetProviderResponse.Marshal(b, m, deterministic)
}
func (m *GetProviderResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetProviderResponse.Merge(m, src)
}
func (m *GetProviderResponse) XXX_Size() int {
	return xxx_messageInfo_GetProviderResponse.Size(m)
}
func (m *GetProviderResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetProviderResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetProviderResponse proto.InternalMessageInfo

func (m *GetProviderResponse) GetProvider() *ProviderView {
	if m != nil {
		return m.Provider
	}
	return nil
}

type FindProvidersRequest struct {
	Filter               string   `protobuf:"bytes,1,opt,name=filter,proto3" json:"filter,omitempty"`
	Page                 int32    `protobuf:"varint,2,opt,name=page,proto3" json:"page,omitempty"`
	Sort                 string   `protobuf:"bytes,3,opt,name=sort,proto3" json:"sort,omitempty"`
	SortDirection        string   `protobuf:"bytes,4,opt,name=sort_direction,json=sortDirection,proto3" json:"sort_direction,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *FindProvidersRequest) Reset()         { *m = FindProvidersRequest{} }
func (m *FindProvidersRequest) String() string { return proto.CompactTextString(m) }
func (*FindProvidersRequest) ProtoMessage()    {}
func (*FindProvidersRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{10}
}

func (m *FindProvidersRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FindProvidersRequest.Unmarshal(m, b)
}
func (m *FindProvidersRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FindProvidersRequest.Marshal(b, m, deterministic)
}
func (m *FindProvidersRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FindProvidersRequest.Merge(m, src)
}
func (m *FindProvidersRequest) XXX_Size() int {
	return xxx_messageInfo_FindProvidersRequest.Size(m)
}
func (m *FindProvidersRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_FindProvidersRequest.DiscardUnknown(m)
}

var xxx_messageInfo_FindProvidersRequest proto.InternalMessageInfo

func (m *FindProvidersRequest) GetFilter() string {
	if m != nil {
		return m.Filter
	}
	return ""
}

func (m *FindProvidersRequest) GetPage() int32 {
	if m != nil {
		return m.Page
	}
	return 0
}

func (m *FindProvidersRequest) GetSort() string {
	if m != nil {
		return m.Sort
	}
	return ""
}

func (m *FindProvidersRequest) GetSortDirection() string {
	if m != nil {
		return m.SortDirection
	}
	return ""
}

type FindProvidersResponse struct {
	Providers            []*ProviderView `protobuf:"bytes,1,rep,name=providers,proto3" json:"providers,omitempty"`
	TotalPages           int32           `protobuf:"varint,2,opt,name=total_pages,json=totalPages,proto3" json:"total_pages,omitempty"`
	XXX_NoUnkeyedLiteral struct{}        `json:"-"`
	XXX_unrecognized     []byte          `json:"-"`
	XXX_sizecache        int32           `json:"-"`
}

func (m *FindProvidersResponse) Reset()         { *m = FindProvidersResponse{} }
func (m *FindProvidersResponse) String() string { return proto.CompactTextString(m) }
func (*FindProvidersResponse) ProtoMessage()    {}
func (*FindProvidersResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_cb47eb53d17c67e0, []int{11}
}

func (m *FindProvidersResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FindProvidersResponse.Unmarshal(m, b)
}
func (m *FindProvidersResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FindProvidersResponse.Marshal(b, m, deterministic)
}
func (m *FindProvidersResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FindProvidersResponse.Merge(m, src)
}
func (m *FindProvidersResponse) XXX_Size() int {
	return xxx_messageInfo_FindProvidersResponse.Size(m)
}
func (m *FindProvidersResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_FindProvidersResponse.DiscardUnknown(m)
}

var xxx_messageInfo_FindProvidersResponse proto.InternalMessageInfo

func (m *FindProvidersResponse) GetProviders() []*ProviderView {
	if m != nil {
		return m.Providers
	}
	return nil
}

func (m *FindProvidersResponse) GetTotalPages() int32 {
	if m != nil {
		return m.TotalPages
	}
	return 0
}

func init() {
	proto.RegisterType((*Provider)(nil), "chotel.Provider")
	proto.RegisterType((*ProviderView)(nil), "chotel.ProviderView")
	proto.RegisterType((*CreateProviderRequest)(nil), "chotel.CreateProviderRequest")
	proto.RegisterType((*CreateProviderResponse)(nil), "chotel.CreateProviderResponse")
	proto.RegisterType((*UpdateProviderRequest)(nil), "chotel.UpdateProviderRequest")
	proto.RegisterType((*UpdateProviderResponse)(nil), "chotel.UpdateProviderResponse")
	proto.RegisterType((*DeleteProviderRequest)(nil), "chotel.DeleteProviderRequest")
	proto.RegisterType((*DeleteProviderResponse)(nil), "chotel.DeleteProviderResponse")
	proto.RegisterType((*GetProviderRequest)(nil), "chotel.GetProviderRequest")
	proto.RegisterType((*GetProviderResponse)(nil), "chotel.GetProviderResponse")
	proto.RegisterType((*FindProvidersRequest)(nil), "chotel.FindProvidersRequest")
	proto.RegisterType((*FindProvidersResponse)(nil), "chotel.FindProvidersResponse")
}

func init() {
	proto.RegisterFile("backend/proto/provider/provider.proto", fileDescriptor_cb47eb53d17c67e0)
}

var fileDescriptor_cb47eb53d17c67e0 = []byte{
	// 615 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xec, 0x55, 0xdd, 0x6e, 0xd3, 0x30,
	0x18, 0x55, 0xb3, 0xad, 0x6b, 0xbe, 0x76, 0xd1, 0x64, 0xd6, 0xca, 0xea, 0x18, 0x4c, 0x11, 0x13,
	0xbb, 0x40, 0x03, 0x95, 0x37, 0x60, 0x7f, 0x12, 0x42, 0xda, 0x54, 0x04, 0x17, 0xdc, 0x54, 0x49,
	0xfc, 0x6d, 0xf3, 0x48, 0xe3, 0x10, 0xbb, 0x45, 0xbb, 0xe0, 0x49, 0x78, 0x1a, 0x9e, 0x8b, 0x1b,
	0x64, 0xc7, 0x6e, 0x97, 0xb4, 0x15, 0x52, 0xc5, 0x25, 0x37, 0xad, 0xbf, 0x73, 0x3e, 0x1f, 0x1f,
	0xdb, 0xc7, 0x0a, 0x1c, 0xc5, 0x51, 0xf2, 0x15, 0x33, 0xf6, 0x3a, 0x2f, 0x84, 0x12, 0xfa, 0x77,
	0xca, 0x19, 0x16, 0xb3, 0xc1, 0x89, 0xc1, 0x49, 0x33, 0xb9, 0x13, 0x0a, 0xd3, 0xf0, 0x97, 0x07,
	0xad, 0x6b, 0x4b, 0x91, 0x00, 0x3c, 0xce, 0x68, 0xe3, 0xb0, 0x71, 0xbc, 0x35, 0xf4, 0x38, 0x23,
	0x04, 0x36, 0xb3, 0x68, 0x8c, 0xd4, 0x3b, 0x6c, 0x1c, 0xfb, 0x43, 0x33, 0x26, 0x7d, 0x68, 0x31,
	0x91, 0x4c, 0xc6, 0x98, 0x29, 0xba, 0x61, 0xf0, 0x59, 0x4d, 0x28, 0x6c, 0x47, 0x8c, 0x15, 0x28,
	0x25, 0xdd, 0x34, 0x94, 0x2b, 0xc9, 0x73, 0x68, 0xe7, 0x42, 0xaa, 0x28, 0x1d, 0x25, 0x82, 0x21,
	0xdd, 0x32, 0x2c, 0x94, 0xd0, 0xa9, 0x60, 0xa8, 0xa7, 0x26, 0x62, 0x92, 0xa9, 0xe2, 0x81, 0x36,
	0xcb, 0xa9, 0xb6, 0x24, 0x3d, 0x68, 0x16, 0x78, 0xcb, 0x45, 0x46, 0xb7, 0x0d, 0x61, 0x2b, 0x6d,
	0x2e, 0xe1, 0xea, 0x81, 0xb6, 0x4a, 0x73, 0x7a, 0x4c, 0xf6, 0xc1, 0xbf, 0x17, 0xf1, 0x28, 0xbf,
	0x13, 0x19, 0x52, 0xbf, 0x74, 0x77, 0x2f, 0xe2, 0x6b, 0x5d, 0x93, 0x03, 0x80, 0x04, 0xd3, 0xd4,
	0xb2, 0x60, 0x58, 0x5f, 0x23, 0x25, 0xbd, 0x07, 0x5b, 0x38, 0x8e, 0x78, 0x4a, 0xdb, 0x86, 0x29,
	0x0b, 0x12, 0x42, 0x47, 0xc4, 0x12, 0x8b, 0x69, 0xa4, 0xb8, 0xc8, 0x24, 0xed, 0x18, 0xb2, 0x82,
	0x85, 0xbf, 0x3d, 0xe8, 0xb8, 0x33, 0xfc, 0xcc, 0xf1, 0xfb, 0xff, 0x73, 0x5c, 0xeb, 0x1c, 0x8d,
	0xf7, 0x02, 0x23, 0x85, 0x8c, 0xee, 0x58, 0xef, 0x65, 0xa9, 0x0f, 0x6b, 0x2c, 0x18, 0xbf, 0xe1,
	0xc8, 0x68, 0x50, 0xda, 0x71, 0x75, 0x78, 0x0e, 0xdd, 0x53, 0xd3, 0xe6, 0xae, 0x60, 0x88, 0xdf,
	0x26, 0x28, 0x15, 0x79, 0x05, 0x2d, 0x17, 0x7a, 0x73, 0x17, 0xed, 0xc1, 0xee, 0x49, 0x99, 0xfa,
	0x93, 0x59, 0xeb, 0xac, 0x23, 0x7c, 0x0f, 0xbd, 0xba, 0x8c, 0xcc, 0x45, 0x26, 0x91, 0xbc, 0x59,
	0xd0, 0xd9, 0xab, 0xeb, 0xe8, 0x5b, 0x7f, 0xa4, 0x75, 0x0e, 0xdd, 0x4f, 0x39, 0xfb, 0x17, 0x96,
	0xea, 0x32, 0x6b, 0x5b, 0x7a, 0x09, 0xdd, 0x33, 0x4c, 0x71, 0xd1, 0x52, 0x2d, 0xab, 0xe1, 0x00,
	0x7a, 0xf5, 0x46, 0xbb, 0x28, 0x85, 0xed, 0x31, 0x4a, 0x19, 0xdd, 0xa2, 0x69, 0xf7, 0x87, 0xae,
	0x0c, 0x5f, 0x00, 0xb9, 0x44, 0xf5, 0x37, 0xe5, 0x4b, 0x78, 0x52, 0xe9, 0x5a, 0x7b, 0x2f, 0x3f,
	0x60, 0xef, 0x82, 0x67, 0xcc, 0xb1, 0xd2, 0x2d, 0xd8, 0x83, 0xe6, 0x0d, 0x4f, 0x95, 0xd5, 0xf1,
	0x87, 0xb6, 0xd2, 0x09, 0xcf, 0xb5, 0x6b, 0xcf, 0x58, 0x31, 0x63, 0x8d, 0x49, 0x51, 0xb8, 0xa7,
	0x67, 0xc6, 0xe4, 0x08, 0x02, 0xfd, 0x3f, 0x62, 0xbc, 0xc0, 0x44, 0x47, 0xd2, 0xbe, 0xbe, 0x1d,
	0x8d, 0x9e, 0x39, 0x30, 0x4c, 0xa1, 0x5b, 0x5b, 0xde, 0xee, 0x64, 0x00, 0xbe, 0xf3, 0x28, 0x69,
	0xe3, 0x70, 0x63, 0xe5, 0x56, 0xe6, 0x6d, 0xfa, 0x41, 0x2b, 0xa1, 0xdf, 0xb3, 0x76, 0x25, 0xad,
	0x45, 0x30, 0xd0, 0xb5, 0x46, 0x06, 0x3f, 0x37, 0x60, 0xd7, 0x4d, 0xfe, 0x88, 0xc5, 0x94, 0x27,
	0x28, 0xc9, 0x15, 0x04, 0xd5, 0xb0, 0x92, 0x03, 0xb7, 0xd0, 0xd2, 0xb7, 0xd0, 0x7f, 0xb6, 0x8a,
	0xb6, 0xd6, 0xaf, 0x20, 0xa8, 0x46, 0x6d, 0x2e, 0xb8, 0x34, 0xc9, 0x73, 0xc1, 0x15, 0x09, 0xbd,
	0x82, 0xa0, 0x1a, 0xa3, 0xb9, 0xe0, 0xd2, 0x1c, 0xce, 0x05, 0x57, 0xa4, 0xef, 0x02, 0xda, 0x8f,
	0xd2, 0x43, 0xfa, 0xae, 0x7d, 0x31, 0x78, 0xfd, 0xfd, 0xa5, 0x9c, 0xd5, 0xf9, 0x00, 0x3b, 0x95,
	0xdb, 0x23, 0x4f, 0x5d, 0xf7, 0xb2, 0x4c, 0xf5, 0x0f, 0x56, 0xb0, 0xa5, 0xda, 0xbb, 0xce, 0x17,
	0x70, 0x77, 0x99, 0xc7, 0x71, 0xd3, 0x7c, 0x5b, 0xdf, 0xfe, 0x09, 0x00, 0x00, 0xff, 0xff, 0x30,
	0xc0, 0x34, 0xc5, 0x84, 0x07, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// ProviderServicesClient is the client API for ProviderServices service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ProviderServicesClient interface {
	CreateProvider(ctx context.Context, in *CreateProviderRequest, opts ...grpc.CallOption) (*CreateProviderResponse, error)
	UpdateProvider(ctx context.Context, in *UpdateProviderRequest, opts ...grpc.CallOption) (*UpdateProviderResponse, error)
	DeleteProvider(ctx context.Context, in *DeleteProviderRequest, opts ...grpc.CallOption) (*DeleteProviderResponse, error)
	GetProvider(ctx context.Context, in *GetProviderRequest, opts ...grpc.CallOption) (*GetProviderResponse, error)
	FindProviders(ctx context.Context, in *FindProvidersRequest, opts ...grpc.CallOption) (*FindProvidersResponse, error)
}

type providerServicesClient struct {
	cc grpc.ClientConnInterface
}

func NewProviderServicesClient(cc grpc.ClientConnInterface) ProviderServicesClient {
	return &providerServicesClient{cc}
}

func (c *providerServicesClient) CreateProvider(ctx context.Context, in *CreateProviderRequest, opts ...grpc.CallOption) (*CreateProviderResponse, error) {
	out := new(CreateProviderResponse)
	err := c.cc.Invoke(ctx, "/chotel.ProviderServices/CreateProvider", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *providerServicesClient) UpdateProvider(ctx context.Context, in *UpdateProviderRequest, opts ...grpc.CallOption) (*UpdateProviderResponse, error) {
	out := new(UpdateProviderResponse)
	err := c.cc.Invoke(ctx, "/chotel.ProviderServices/UpdateProvider", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *providerServicesClient) DeleteProvider(ctx context.Context, in *DeleteProviderRequest, opts ...grpc.CallOption) (*DeleteProviderResponse, error) {
	out := new(DeleteProviderResponse)
	err := c.cc.Invoke(ctx, "/chotel.ProviderServices/DeleteProvider", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *providerServicesClient) GetProvider(ctx context.Context, in *GetProviderRequest, opts ...grpc.CallOption) (*GetProviderResponse, error) {
	out := new(GetProviderResponse)
	err := c.cc.Invoke(ctx, "/chotel.ProviderServices/GetProvider", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *providerServicesClient) FindProviders(ctx context.Context, in *FindProvidersRequest, opts ...grpc.CallOption) (*FindProvidersResponse, error) {
	out := new(FindProvidersResponse)
	err := c.cc.Invoke(ctx, "/chotel.ProviderServices/FindProviders", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ProviderServicesServer is the server API for ProviderServices service.
type ProviderServicesServer interface {
	CreateProvider(context.Context, *CreateProviderRequest) (*CreateProviderResponse, error)
	UpdateProvider(context.Context, *UpdateProviderRequest) (*UpdateProviderResponse, error)
	DeleteProvider(context.Context, *DeleteProviderRequest) (*DeleteProviderResponse, error)
	GetProvider(context.Context, *GetProviderRequest) (*GetProviderResponse, error)
	FindProviders(context.Context, *FindProvidersRequest) (*FindProvidersResponse, error)
}

// UnimplementedProviderServicesServer can be embedded to have forward compatible implementations.
type UnimplementedProviderServicesServer struct {
}

func (*UnimplementedProviderServicesServer) CreateProvider(ctx context.Context, req *CreateProviderRequest) (*CreateProviderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateProvider not implemented")
}
func (*UnimplementedProviderServicesServer) UpdateProvider(ctx context.Context, req *UpdateProviderRequest) (*UpdateProviderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateProvider not implemented")
}
func (*UnimplementedProviderServicesServer) DeleteProvider(ctx context.Context, req *DeleteProviderRequest) (*DeleteProviderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteProvider not implemented")
}
func (*UnimplementedProviderServicesServer) GetProvider(ctx context.Context, req *GetProviderRequest) (*GetProviderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetProvider not implemented")
}
func (*UnimplementedProviderServicesServer) FindProviders(ctx context.Context, req *FindProvidersRequest) (*FindProvidersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FindProviders not implemented")
}

func RegisterProviderServicesServer(s *grpc.Server, srv ProviderServicesServer) {
	s.RegisterService(&_ProviderServices_serviceDesc, srv)
}

func _ProviderServices_CreateProvider_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateProviderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProviderServicesServer).CreateProvider(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chotel.ProviderServices/CreateProvider",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProviderServicesServer).CreateProvider(ctx, req.(*CreateProviderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProviderServices_UpdateProvider_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateProviderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProviderServicesServer).UpdateProvider(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chotel.ProviderServices/UpdateProvider",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProviderServicesServer).UpdateProvider(ctx, req.(*UpdateProviderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProviderServices_DeleteProvider_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteProviderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProviderServicesServer).DeleteProvider(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chotel.ProviderServices/DeleteProvider",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProviderServicesServer).DeleteProvider(ctx, req.(*DeleteProviderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProviderServices_GetProvider_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetProviderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProviderServicesServer).GetProvider(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chotel.ProviderServices/GetProvider",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProviderServicesServer).GetProvider(ctx, req.(*GetProviderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProviderServices_FindProviders_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FindProvidersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProviderServicesServer).FindProviders(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chotel.ProviderServices/FindProviders",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProviderServicesServer).FindProviders(ctx, req.(*FindProvidersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ProviderServices_serviceDesc = grpc.ServiceDesc{
	ServiceName: "chotel.ProviderServices",
	HandlerType: (*ProviderServicesServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateProvider",
			Handler:    _ProviderServices_CreateProvider_Handler,
		},
		{
			MethodName: "UpdateProvider",
			Handler:    _ProviderServices_UpdateProvider_Handler,
		},
		{
			MethodName: "DeleteProvider",
			Handler:    _ProviderServices_DeleteProvider_Handler,
		},
		{
			MethodName: "GetProvider",
			Handler:    _ProviderServices_GetProvider_Handler,
		},
		{
			MethodName: "FindProviders",
			Handler:    _ProviderServices_FindProviders_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "backend/proto/provider/provider.proto",
}
