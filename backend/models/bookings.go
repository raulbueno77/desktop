package models

type NewBooking struct {
	Id			int32		`json:"id"`
	Number		string		`json:"number"`
	RoomName	string		`json:"roomName"`
	CheckIn		string		`json:"checkIn"`
	CheckOut	string		`json:"checkOut"`
	CheckInDate	string		`json:"checkInDate"`
	Price		float32		`json:"price"`
	Client		Client		`json:"client"`
}

type Guest struct {
	Id			int32				`json:"id"`
	BookingId	int32				`json:"booking_id"`
	FirstName			string 		`json:"first_name"`
	LastName			string		`json:"last_name"`
	SecondLastName		string		`json:"second_last_name"`
	BirthDate			string		`json:"birth_date"`
	Document			string		`json:"document"`
	DocumentType		string		`json:"document_type"`
	Gender				string		`json:"gender"`
	Country				string		`json:"country"`
	DocumentDate		string		`json:"document_date"`
	CheckIn				string		`json:"check_in"`

}