package models

type Select struct {
	Text	string	`json:"text"`
	Value	int32	`json:"value"`
}

type File struct {
	Data	[]byte	`json:"data"`
	Name	string 	`json:"name"`
}

type Event struct {
	Title	string	`json:"title"`
	Start	string	`json:"start"`
	End		string	`json:"end"`
	Color	string	`json:"color"`
}