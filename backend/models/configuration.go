package models

type ServerGrpc struct {
	Host	string
	Port	string
}

type OtherConfig struct {
	PdfExecutable	string
}

type Configuration struct {
	Grpc 			ServerGrpc
	Others			OtherConfig
	PdfExecutable	string
}