package models


type SaleInvoice struct {
	Id					int32		` json:"id"`
	ClientId			int32		` json:"client_id"`
	FirstName			string 		`json:"first_name"`
	LastName			string		`json:"last_name"`
	SecondLastName		string		`json:"second_last_name"`
	Document			string		`json:"document"`
	Address				string		`json:"address"`
	PostalCode			string		`json:"postal_code"`
	Country				string		`json:"country"`
	Region				string		`json:"region"`
	City				string		`json:"city"`
	HomePhone			string		`json:"home_phone"`
	CellPhone			string		`json:"cell_phone"`
	Email				string		`json:"email"`
	Observations		string		`json:"observations"`
	Date				string		`json:"date"`
	Total				float32		`json:"total"`
	Number				string		`json:"number"`
	SerialInvoiceId		int32		`json:"serial_invoice_id"`
	Details				*[]SaleInvoceDetail `json:"details"`
}

type SaleInvoceDetail struct {
	Id				int32		`json:"id"`
	SaleInvoiceId	int32		`json:"sale_invoice_id"`
	Reference		string		`json:"reference"`
	Description		string		`json:"description"`
	Units			float32		`json:"units"`
	Tax				float32		`json:"tax"`
	Price			float32		`json:"price"`
	Total			float32		`json:"total"`
}

type SerialInvoice struct {
	Id		int32		`json:"id"`
	Name	string		`json:"name"`
	Serial	string		`json:"serial"`
	Record	string		`json:"record"`
}

type SaleInvoiceListView struct {
	Id			int32		`json:"id"`
	Date		string		`json:"date"`
	Document	string		`json:"document"`
	Name		string		`json:"name"`
	Total		float32		`json:"total"`
}

type SaleInvoicesList struct {
	SaleInvoices		[]SaleInvoiceListView 	`json:"sale_invoices"`
	TotalPages			int32	 				`json:"total_pages"`
	SelectPages 		[]Select 				`json:"select_pages"`
}
