package models

import hotelpb "cehotel/backend/proto/hotel"

type Hotel struct {
	Id			int32		`json:"id"`
	Name		string		`json:"name"`
	Document	string		`json:"document"`
	Reference	string		`json:"reference"`
	Phone		string		`json:"phone"`
	Email		string		`json:"email"`
	Address		string		`json:"address"`
	PostalCode	string		`json:"postal_code"`
	Country		string		`json:"country"`
	Region		string		`json:"region"`
	City		string		`json:"city"`
	Logo		*LogoHotel	`json:"logo"`
	DeleteLogo	bool		`json:"delete_logo"`
}

type HotelShort struct {
	Id		int32		`json:"id"`
	Name 	string		`json:"name"`
}

type LogoHotel struct {
	Id		string	`json:"id"`
	Src		string	`json:"src"`
	New		bool	`json:"new"`
}

func (h *Hotel) Hydrate (hotel *hotelpb.HotelView, logo *LogoHotel) {
	h.Id = hotel.GetId()
	h.Name = hotel.GetName()
	h.Document = hotel.GetDocument()
	h.Reference = hotel.GetReference()
	h.Phone = hotel.GetPhone()
	h.Email = hotel.GetEmail()
	h.Address = hotel.GetAddress()
	h.PostalCode = hotel.GetPostalCode()
	h.Country = hotel.GetCountry()
	h.Region = hotel.GetRegion()
	h.City = hotel.GetCity()
	h.Logo = logo
}

func (h *Hotel) Parse () *hotelpb.Hotel {
	return &hotelpb.Hotel{
		Id:                   h.Id,
		Name:                 h.Name,
		Document:             h.Document,
		Reference:            h.Reference,
		Phone:                h.Phone,
		Email:                h.Email,
		Address:              h.Address,
		PostalCode:           h.PostalCode,
		Country:              h.Country,
		Region:               h.Region,
		City:                 h.City,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}