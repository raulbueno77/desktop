package models

import (
	providerpb "cehotel/backend/proto/provider"
)

type Provider struct {
	Id                   int32    `json:"id"`
	Name            	 string   `json:"name"`
	Document             string   `json:"document"`
	Address              string   `json:"address"`
	PostalCode           string   `json:"postal_code"`
	Country              string   `json:"country"`
	Region               string   `json:"region"`
	City                 string   `json:"city"`
	JobPhone             string   `json:"job_phone"`
	CellPhone            string   `json:"cell_phone"`
	Email                string   `json:"email"`
	Observations         string   `json:"observations"`
}

type ListProviderView struct {
	Id                   int32    `json:"id"`
	Name	             string   `json:"name"`
	Document             string   `json:"document"`
	Place                string   `json:"place"`
	Phones               string   `json:"phones"`
	Email                string   `json:"email"`
}

type ProviderList struct {
	Provider	[]ListProviderView 	`json:"providers"`
	TotalPages	int32	 			`json:"total_pages"`
	SelectPages []Select 			`json:"select_pages"`
}

func (c *Provider) Hydrate (prov *providerpb.ProviderView) {
	c.Id = prov.GetId()
	c.Name = prov.GetName()
	c.Document = prov.GetDocument()
	c.Address = prov.GetAddress()
	c.PostalCode = prov.GetPostalCode()
	c.Country = prov.GetCountry()
	c.Region = prov.GetRegion()
	c.City = prov.GetCity()
	c.JobPhone = prov.GetJobPhone()
	c.CellPhone = prov.GetCellPhone()
	c.Email = prov.GetEmail()
	c.Observations = prov.GetObservations()
}

func (c *Provider) Parse () *providerpb.Provider {
	return &providerpb.Provider{
		Id:                 c.Id,
		Name:            	c.Name,
		Document:           c.Document,
		Address:            c.Address,
		PostalCode:         c.PostalCode,
		Country:            c.Country,
		Region:             c.Region,
		City:               c.City,
		JobPhone:           c.JobPhone,
		CellPhone:          c.CellPhone,
		Email:              c.Email,
		Observations:       c.Observations,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}
