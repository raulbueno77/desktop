package models

import (
	clientpb "cehotel/backend/proto/client"
	"cehotel/backend/utils"
	"strings"
)

type Client struct {
	Id                   int32    `json:"id"`
	FirstName            string   `json:"first_name"`
	LastName             string   `json:"last_name"`
	SecondLastName       string   `json:"second_last_name"`
	BirthDate            string   `json:"birth_date"`
	Document             string   `json:"document"`
	DocumentType         string   `json:"document_type"`
	Gender               string   `json:"gender"`
	Address              string   `json:"address"`
	PostalCode           string   `json:"postal_code"`
	Country              string   `json:"country"`
	Region               string   `json:"region"`
	City                 string   `json:"city"`
	HomePhone            string   `json:"home_phone"`
	CellPhone            string   `json:"cell_phone"`
	Email                string   `json:"email"`
	CreditCardNumber     string   `json:"credit_card_number"`
	CreditCardMonth      string   `json:"credit_card_month"`
	CreditCardYear       string   `json:"credit_card_year"`
	CreditCardSecurity   string   `json:"credit_card_security"`
	Observations         string   `json:"observations"`
	DocumentDate		 string	  `json:"document_date"`
}

type ListClientView struct {
	Id                   int32    `json:"id"`
	Name	             string   `json:"name"`
	Document             string   `json:"document"`
	Place                string   `json:"place"`
	Phones               string   `json:"phones"`
	Email                string   `json:"email"`
}

type ClientList struct {
	Clients		[]ListClientView `json:"clients"`
	TotalPages	int32	 `json:"total_pages"`
	SelectPages []Select `json:"select_pages"`
}

func (c *Client) Hydrate (cli *clientpb.ClientView) {
	c.Id = cli.GetId()
	c.FirstName = cli.GetFirstName()
	c.LastName = cli.GetLastName()
	c.SecondLastName = cli.GetSecondLastName()
	c.BirthDate = cli.GetBirthDate()
	c.Document = cli.GetDocument()
	c.DocumentType = cli.GetDocumentType()
	c.Gender = cli.GetGender()
	c.Address = cli.GetAddress()
	c.PostalCode = cli.GetPostalCode()
	c.Country = cli.GetCountry()
	c.Region = cli.GetRegion()
	c.City = cli.GetCity()
	c.HomePhone = cli.GetHomePhone()
	c.CellPhone = cli.GetCellPhone()
	c.Email = cli.GetEmail()
	c.CreditCardNumber = cli.GetCreditCardNumber()
	c.CreditCardYear = cli.GetCreditCardYear()
	c.CreditCardMonth = cli.GetCreditCardMonth()
	c.CreditCardSecurity = cli.GetCreditCardSecurity()
	c.Observations = cli.GetObservations()
	c.DocumentDate = cli.GetDocumentDate()
}

func (c *Client) Parse () *clientpb.Client {
	return &clientpb.Client{
		Id:                   c.Id,
		FirstName:            c.FirstName,
		LastName:             c.LastName,
		SecondLastName:       c.SecondLastName,
		BirthDate:            c.BirthDate,
		Document:             utils.FormatDocument(c.Document),
		DocumentType:         c.DocumentType,
		Gender:               c.Gender,
		Address:              c.Address,
		PostalCode:           c.PostalCode,
		Country:              c.Country,
		Region:               c.Region,
		City:                 c.City,
		HomePhone:            c.HomePhone,
		CellPhone:            c.CellPhone,
		Email:                c.Email,
		CreditCardNumber:     c.CreditCardNumber,
		CreditCardMonth:      c.CreditCardMonth,
		CreditCardYear:       c.CreditCardYear,
		CreditCardSecurity:   c.CreditCardSecurity,
		Observations:         c.Observations,
		Provisional:          func () bool {
			if strings.TrimSpace(c.Document) == "" {
				return true
			} else {
				return false
			}
		}(),
		DocumentDate: 		  c.DocumentDate,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}
