package models

type LoginResponse struct {
	Token		string			`json:"token"`
	UserId		int32			`json:"user_id"`
	Username	string			`json:"username"`
	Fullname	string			`json:"fullname"`
	Role		string			`json:"role"`
	Hotels		[]*HotelShort	`json:"hotels"`
}

type UserGroupsSelect struct {
	Text	string	`json:"text"`
	Value	int32	`json:"value"`
}

type User struct {
	Id                   int32                `json:"id"`
	Username             string               `json:"username"`
	Firstname            string               `json:"firstname"`
	Lastname             string               `json:"lastname"`
	Password             string               `json:"password"`
	Email                string               `json:"email"`
	Archived             bool                 `json:"archived"`
	UserGroupId          int32                `json:"user_group_id"`
	Expiration           string 			  `json:"expiration"`
	Description          string				  `json:"description"`
	Hotels				 []*int32			  `json:"hotels"`
	Group				 *string			  `json:"group"`
}
