package models

import roompb "cehotel/backend/proto/room"

type RoomType struct {
	Id				int32			`json:"id"`
	Name			string			`json:"name"`
	DisabledWeb		bool			`json:"disabled_web"`
	Pictures		[]*RoomPictures	`json:"pictures"`
	HotelId			int32			`json:"hotel_id"`
	BasePrice    	float32			`json:"base_price"`
}

type RoomTypeList struct {
	Id			int32		`json:"id"`
	Name		string		`json:"name"`
	MainPicture	*string		`json:"main_picture"`
}

type RoomPictures struct {
	Id		string	`json:"id"`
	Main	bool	`json:"main"`
	Src		string	`json:"src"`
	New		bool	`json:"new"`
}

func (t* RoomType) Hydrate (roomType *roompb.RoomTypeSingleView, pictures []*RoomPictures) {
	t.Id = roomType.GetId()
	t.Name = roomType.GetName()
	t.DisabledWeb = roomType.GetDisabledWeb()
	t.HotelId = roomType.GetHotelId()
	t.Pictures = pictures
	t.BasePrice = roomType.GetBasePrice()
}

func (t *RoomType) Parse () *roompb.RoomType {
	return &roompb.RoomType{
		Id:                   t.Id,
		Name:                 t.Name,
		DisabledWeb:          t.DisabledWeb,
		HotelId:              t.HotelId,
		BasePrice:            t.BasePrice,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

type Attribute struct {
	Id		int32	`json:"id"`
	Name	string	`json:"name"`
}

type Room struct {
	Id			int32			`json:"id"`
	Name		string			`json:"name"`
	Floor		string			`json:"floor"`
	BasePrice	float32			`json:"base_price"`
	Attributes	[]*int32		`json:"attributes"`
	IdType		int32			`json:"id_type"`
	IdHotel		int32			`json:"id_hotel"`
	Disabled	bool			`json:"disabled"`
}

type RoomView struct {
	Id			int32	`json:"id"`
	Name		string	`json:"name"`
	Floor		string	`json:"floor"`
	Type		string	`json:"type"`
	Disabled	bool	`json:"disabled"`
}

type RoomList struct {
	Rooms		[]RoomView 	`json:"rooms"`
	TotalPages	int32	 	`json:"total_pages"`
	SelectPages []Select 	`json:"select_pages"`
}

func (r *Room) Hydrate (room *roompb.RoomView) {
	r.Id = room.GetId()
	r.Name = room.GetName()
	r.Floor = room.GetFloor()
	r.Attributes = func () []*int32 {
		attributes := room.GetAttributes()
		
		result := []*int32{}
		
		for _, attr := range attributes {
			result = append(result, &attr.Id)	
		}
		
		return result
	}()
	r.IdHotel = room.GetHotel().GetId()
	r.IdType = room.GetRoomType().GetId()
	r.Disabled = room.GetDisabled()
}

func (r *Room) Parse () *roompb.Room {
	return &roompb.Room{
		Id:                   r.Id,
		Name:                 r.Name,
		Floor:                r.Floor,
		Attributes:           func () []int32 {
			attributes := []int32{}
			
			for _, attr := range r.Attributes {
				attributes = append(attributes, *attr)
			}
			
			return attributes
		}(),
		IdType:               r.IdType,
		IdHotel:              r.IdHotel,
		Disabled:             r.Disabled,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}