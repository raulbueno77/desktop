module cehotel

go 1.16

require (
	github.com/golang/protobuf v1.4.2
	github.com/johnfercher/maroto v0.28.0
	github.com/jung-kurt/gofpdf v1.16.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/wailsapp/wails v1.16.5
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/grpc v1.32.0
	gopkg.in/ini.v1 v1.61.0
)
